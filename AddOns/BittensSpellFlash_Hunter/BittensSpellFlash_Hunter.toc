## Interface: 50400
## Version: 50400.2.2
## Author: Xemnosyst
## Title: Bitten's SpellFlash: Hunter
## Notes: Replaces Blizzard's default proc highlighting to flash a maximum dps rotation.
## URL: http://www.curse.com/addons/wow/bittens-spellflash-hunter
## Dependencies: SpellFlash
## OptionalDeps: BittensSpellFlashLibrary
## LoadOnDemand: 1
## X-SpellFlashAddon-LoadWith: Hunter
## X-Curse-Packaged-Version: 50400.2.2
## X-Curse-Project-Name: Bitten's SpellFlash: Hunter
## X-Curse-Project-ID: bittens-spellflash-hunter
## X-Curse-Repository-ID: wow/bittens-spellflash-hunter/mainline
utils\BittensUtils.xml
bsl\BittensSpellFlashLibrary.xml
src\Localization.lua
src\IDs.lua
src\Spells.lua
src\Rotations.lua
src\Options.lua
