== Version 50400.1.2
* Destruction: Shadowburn flashing is now optional (yellow) at certain times, to let you pool its usage for procs.
* Destruction: Stopped Conflagrate from flashing as optional when it's better to use Chaos Bolt.
* Destruction: Added flashing yor Chaos Bolt / Shadowburn when out of mana for Incinerate.
* All: Flashes WoW's 9th Anniversary Celebration Package.

== Version 50400.1.1
* Destruction: Updated the priorities based on new research.

== Version 50400.1.0
* Affliction: Continues to flash Haunt as if you have Dark Soul while at least 2 of your dots still have the haste bonus from it.
* Affliction: Waits for 4 shards before flashing Haunt outside of Dark Soul if you take Archimonde's Darkness.
* Affliction: Added prediction flashing for Soulburn when Dark Soul: Misery is flashing, and for Soul Swap when Soulburn is flashing.
* Affliction: Updates for some internal changes Blizzard made with 5.4.
* Demonology: No longer extends Corruption with Void Ray.
* Demonology: Removed Harvest Life from the AoE rotation.
* Demonology: Updated ranges for the new behavior of Mannoroth's Fury.
* All: Modified Dark Soul flashing when you take Archimonde's Darkness.
* All: Smoothed out some quirky flashing.

== Version 3.7.1
* Demonology: Now accounts for Mannoroth's Fury when deciding which spells to flash in AoE mode (which only affects Hellfire and Immolation Aura).
* Demonology: Now flashes Spell Lock and Optical Blast if for some reason you are using your Felhunter / Observer.
* All: Now flashes Devour Magic when you can dispel your target, and Clone Magic when you can steal your target's buff.
* All: Now flashes food buffs.
* All: Soulstone no longer flashes out of combat until you target an enemy.

== Version 3.7.0
Update for patch 5.3.
* Destruction: Updated Fire and Brimstone flashing to match its behavior in 5.3.
* Destruction: Bumped Rain of Fire up in the AoE priority list.

== Version 3.6.7
* Demonology & Destruction: Demonic Fury Burning Ember calculations now account for T15 set bonuses.
* Destruction: Bugfix - There was a situation when Chaos Bolt could flash when you would not have enough Burning Embers to cast it.

== Version 3.6.6
* Update to the latest Bitten's SpellFlash Library, to fix a bug when Metamorphosis should flash to be canceled.

== Version 3.6.5
* Affliction: Bugfix for a Lua error.

== Version 3.6.4
NOTE: This version includes a new version of Bitten's SpellFlash Library that I tried to test well, but if you find any issues please let me know right away.
* Destruction: small adjustment to Rain of Fire.  This release is mostly about the update to the library.

== Version 3.6.3
NOTE: Your options will be reset when you upgrade to this version.
* Upgraded to the latest Bitten's SpellFlash Library.
* Several other internal changes.
* Destruction: Factors in the new Glyph of Ember Tap when deciding whether it should flash.
* All: Now flashes Dark Intent if anyone in your raid needs the stamina buff (or the spell power buff, as always).
* All: No longer flashes Soulstone if you are queued for a dungeon/raid.

== Version 3.6.2
* Bear with me - I'm learning this new packaging system.  I think this time the debug output should be removed.

== Version 3.6.1
* No change, just releasing out of beta (see last version for the real changes).

== Version 3.6.0 beta
* Repackaging so that Bitten's SpellFlash Library is included with the addon, instead of downloaded separately.
* Destruction: Added Rain of Fire to the single target rotation, which sims as small dps increase.
* Destruction: Added fancy, colored Immolate flashing for Pandemic, like Affliction dots.

== Version 3.5.1
* Affliction & Demonology: Life Tap flashes a little less aggressively when solo.
* Demonology: Bugfix - Shadow Bolt was not flashing if glyphed.
* All: Added Soulshatter flashing, in red, if you pull aggro.

== Version 3.5.0
* Affliction & Demonology: Life Tap now flashes out of combat when you can use the mana.
* Demonology: Added an AoE rotation.
* Demonology: Researched and updated the single target rotation for slightly higher dps.
* Demonology: Doom now flashes pretty colors like DoTs for Affliction.
* Demonology: The next priority after Doom now also flashes, in case your target will not live long enough to be worth casting Doom.
* Demonology: Closer tracking of Demonic Fury.
* Demonology: Made Felstorm and Wrathstorm flashing behave a little better (they were flashing early at times).
* Demonology: Bugfix - Grimoire: Felhunter was not flashing.
* Demonology: Bugfix - Aura of the Elements was flashing even when it was on cooldown.

== Version 3.4.1
* Affliction: DoT dps is now tracked per-target, so the pretty colors now work when multi-dotting.
* Affliction: Bugfix (I hope) - Corruption was sometimes never flashing because of a bug in Soulburn + Seed of Corruption detection.

== Version 3.4.0
* Affliction: Super-cool green/yellow/red flashing for DoTs now.  Check the description page for what they mean and how you should respond.
* Affliction: Traded the priority of Corruption and Unstable Affliction unless corruption will expire otherwise.

== Version 3.3.0
* Affliction: Tweaked the rotation according to more research in Simulationcraft.  DoT logic is more sophisticated, and should produces slightly higher dps.
* Affliction: Made better use of yellow flashes and small flashes for DoTs.  Consult the description page for more info on what they mean.
* Affliction: No longer flashes Corruption if you have an empowered Seed of Corruption pending.  (Warning: it will get confused if chain cast Seed of Corruption.)
* All: Now flashes Underwater Breathing.

== Version 3.2.3
* Demonology: Bugfix - Touch of Chaos did not always flash when it should.

== Version 3.2.2
* Affliction: Bugfix - The cast time for Unstable Affliction was not being factored in correctly, causing it to flash later than it should.

== Version 3.2.1
* All: Bugfix - Dark Regeneration flashing was broken when your pet wasn't out.
