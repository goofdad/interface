------------------------------------------------------------------------
r255 | stencil | 2013-12-17 09:28:46 +0000 (Tue, 17 Dec 2013) | 1 line
Changed paths:
   A /tags/2.9.3 (from /trunk:254)

Tagging as release 2.9.3
------------------------------------------------------------------------
r253 | stencil | 2013-11-17 11:05:44 +0000 (Sun, 17 Nov 2013) | 1 line
Changed paths:
   M /trunk
   M /trunk/.pkgmeta
   M /trunk/Ailo.lua
   M /trunk/Ailo.toc

Improvements to currency tracking. Warforged seal count should no longer be lost when weekly quest is reset. Removed dependency on libsynctime.
------------------------------------------------------------------------
