## Interface: 50400
## Version: 50400.2.0
## Author: Xemnosyst
## Title: Bitten's SpellFlash: Warrior
## Notes: Replaces Blizzard's default proc highlighting to flash 1) a maximum dps rotation, or 2) a maximum survivability tanking rotation.
## URL: http://www.curse.com/addons/wow/bittens-spellflash-warrior
## Dependencies: SpellFlash
## OptionalDeps: BittensSpellFlashLibrary
## LoadOnDemand: 1
## X-SpellFlashAddon-LoadWith: Warrior
## X-Curse-Packaged-Version: 50400.2.0
## X-Curse-Project-Name: Bitten's SpellFlash: Warrior
## X-Curse-Project-ID: bittens-spellflash-warrior
## X-Curse-Repository-ID: wow/bittens-spellflash-warrior/mainline
utils\BittensUtils.xml
bsl\BittensSpellFlashLibrary.xml
src\Localization.lua
src\IDs.lua
src\Spells.lua
src\Rotations.lua
src\Options.lua
