== Version 50400.1.1
* All: Flashes WoW's 9th Anniversary Celebration Package.

== Version 50400.1.0
* Fire: Updated the combustion monitor to reflect the nerf from this patch.  You should update your "Minumum Combustion total damage" accordingly.
* Fire & Frost: No longer tries to re-apply Rune of Power or Invoker's Energy early when solo.  It always waits until it falls off before flashing, because you might just finish your enemy during that time.
* Frost: Remove the priority for stack Frostbolt's debuff, since that debuff was removed.

== Version 3.4.3
* Fire: Bugfix - update to the latest Bitten's SpellFlash Library to fix an issue w/ Presence of Mind.

== Version 3.4.2
* All: Added flashing for Ice Barrier when solo.

== Version 3.4.1
* Frost: When solo, Icy Veins no longer waits for 3 stacks of the Frostbolt debuff when glyphed.
* All: Added flashing for food buffs.
* All: Rune of Power and Evocation now flash before combat when an enemy is targeted for your level 90 talent buff.
* All: When solo, Rune of Power and Evocation no longer flash in combat until their buffs completely expire.

== Version 3.4.0
Updated for patch 5.3.
* Arcane: Updated the AoE rotation based on new research.

== Version 3.3.3
* Arcane: Don't flash Presence of Mind if Arcane Blast's cast time is below 1.2 seconds (instead you should save it until the haste buff(s) wear off).
* Arcane: Delay Presence of Mind until immediately before Alter Time if the cooldowns are close.
* Arcane: Adjusted the small/yellow/green flashing of Presence of Mind and Arcane Missiles that indicate what to do if solo and/or not using Arcane Power immediately.
* Arcane: Only flash Evocation when at zero stacks of Arcane Charge (sims at higher dps).

== Version 3.3.2
* Arcane: Added movement fallthrough flashing.
* Arcane: Now flashes for you to apply either Frost or Mage armor.
* Fire: Updated the fire rotation w/ new research, including adding Alter Time.
* Fire: Scorch is now flashed Orange when you are moving, to be consistent w/ the other specs.
* Frost: Played with the conditions for Freeze and Deep Freeze when solo.
* Frost: Bugfix in the conditions for the higher priority Rune of Power and Invocation.
* All: Bugfix - the duration of Rune of Power was not calculated correctly, causing it to flash much more than it should.

== Version 3.3.1-beta
* Fire & Frost: No longer flash Conjure Mana Gem unless you take Invocation.
* Frost: Now flash Mana Gem before resorting to Evocation when under 10% mana.
* Frost: Bugfix - Rune of Power was broken.
* All: Bugfix - Nether Tempest was broken.

== Version 3.3.0-beta
* Frost: Updating the rotation, including the addition of Alter Time.
* All: Added flashing for the heal from Cold Snap.

== Version 3.2.3
* Frost: Added flashing for Deep Freeze.
* Frost: Ice Lance now flashes at its highest priority when the target has Deep Freeze.
* Frost: Frostbolt no longer flashes at a higher priority to stacks its debuff when solo.

== Version 3.2.2
NOTE: This version includes a new version of Bitten's SpellFlash Library that I tried to test well, but if you find any issues please let me know right away.

== Version 3.2.1
* Frost: No longer flashes Freeze when targeting a boss.
* Frost: Improved the timing of Freeze so it never flashes before its cooldown is ready.

== Version 3.2.0
NOTE: Your options will be reset when you upgrade to this version.
* Repackaging so that Bitten's SpellFlash Library is included with the addon, instead of downloaded separately.
* Several other internal changes.
* Arcane: Updated rotations (both single target and AoE) for 5.2.

== Version 3.1.2
* Arcane: Arcane Power no longer flashes if you have the talent Incanter's Ward, but none of its benefits.
* Arcane: Bugfix - Arcane Power was never flashing if you had the talent Rune of Power.
* Arcane: Bugfix - minor fix in the AoE rotation.

== Version 3.1.1
* All: Bugfix - Frost Bomb was broken in the last release.  Sorry!

== Version 3.1.0
* Arcane: By popular demand, Arcane Blast now stays at its higher priority until you have 4 stacks of Arcane Charge (simulations showed no difference in dps).
* All: There is now a "Solo Mode" option that adjusts things when you are not in a group.  See the (new) website for more details.

== Version 3.0.2
* Arcane: now flashes Arcane Barrage if Arcane Charge would fall off otherwise.

== Version 3.0.1 beta
* Arcane: added an AoE rotation.
* Arcane: no longer prioritize Arcane Blast based on number of Arcane Charges.

== Version 3.0.0 beta
* Arcane: pretty much reworked the whole rotation.
* All: Living Bomb will refresh within the last tick (instead of waiting until it expires).
* All: Presence of Mind will not flashes during Alter Time.

== Version 2.13.0
* Fire: Lowered the priority of Nether Tempest / Living Bomb.  Simulations showed a dps improvement.
* Fire: Worked a bit on the Heating Up / Pyroblast! detection, including adjusting some lag tolerances.  I believe it is an improvement.
* Fire: Removed the 3rd line from the combustion monitor.

== Version 2.12.4
* Fire: Fixed the combustion monitor to reflect changes in 5.1.0.
