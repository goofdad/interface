local addonName, vars = ...
_G[addonName] = vars
local addon = vars

local rev = tonumber(("$Revision: 7 $"):match("%d+"))
local function msg(...)
    print("\124cff00ff00CalendarKeyboardFixer\124r: ",...)
end
local debug_enabled = false
local function debug(...)
  if debug_enabled then
    msg(...)
  end
end

--[[
  Here is one LUA stack for the problem we are fixing: (see Blizzard_Calendar/Blizzard_Calendar.lua)
 
  1: CalendarCreateEventDescriptionEdit:ClearFocus()
  2: CalendarCreateEventFrame_Update  ( CalendarCreateEventFrame.mode == "edit" ) 
  3: CalendarCreateEventFrame_OnEvent  ( event == "GUILD_ROSTER_UPDATE" )
      
  CalendarCreateEventFrame_OnLoad(self) => self:RegisterEvent("GUILD_ROSTER_UPDATE");

  We intervene by wrapping stack frame 2 and prevent changes to the focus
--]]        

local function call_dummy(self,...)
  debug("call_dummy")
end

addon.call_blacklist = {
  "ClearFocus",
  "SetText",
  "SetCursorPosition",
  "HighlightText",
}

local function wrapped_update()
  debug("Called CalendarCreateEventFrame_Update")
  local focus = GetCurrentKeyBoardFocus()
  local cpos, spos
  local sf = CalendarCreateEventDescriptionScrollFrame
  addon.focusfns = addon.focusfns or {}
  wipe(addon.focusfns)
  -----------------------------------
  -- SAVE STATE
  -----------------------------------
  if focus then 
     if focus.GetCursorPosition then
       cpos = focus:GetCursorPosition();
     end
     if focus == CalendarCreateEventDescriptionEdit and sf and sf.GetVerticalScroll then
       spos = sf:GetVerticalScroll();
     elseif focus == CalendarCreateEventTitleEdit then
       -- 
     else -- unrecognized focus 
       focus = nil
     end
     if focus then
       -- Some OnEditFocusLost/Gained scripts change the highlighted text
       -- we apparently cannot query the EditBox highlighted text to save/restore,
       -- so instead we temporarily disable the problematic methods
       for _,methodname in ipairs(addon.call_blacklist) do
         if focus[methodname] then
	   addon.focusfns[methodname] = focus[methodname]
	   focus[methodname] = call_dummy
	 end
       end
     end
  end

  -----------------------------------
  -- CALL BLIZZARD UPDATE
  -----------------------------------
  addon.raw_update() 

  -----------------------------------
  -- RESTORE STATE
  -----------------------------------
  local nfocus = GetCurrentKeyBoardFocus()
  if focus and nfocus ~= focus then -- some other method stole our focus
    debug("Lost focus!! Fixing...")
    focus:SetFocus()
  end
  if focus then
    for methodname, method in pairs(addon.focusfns) do
      focus[methodname] = method
    end
    wipe(addon.focusfns)
    if cpos and focus.SetCursorPosition and (focus:GetCursorPosition() ~= cpos) then
       focus:SetCursorPosition(cpos)
    end
    if spos and sf and sf.SetVerticalScroll and (sf:GetVerticalScroll() ~= spos) then 
       sf:SetVerticalScroll(spos)
    end
  end
end

local function UpdateHooks()
  if addon.hookdone then return end

  if not CalendarCreateEventFrame or 
     not CalendarCreateEventFrame_Update then 
     return 
  end
  addon.raw_update = CalendarCreateEventFrame_Update
  CalendarCreateEventFrame_Update = wrapped_update
  -- .update is only used by CalendarFrame_ShowEventFrame, 
  -- which we do NOT want to hook because it indicates a 
  -- change in which event is being displayed, so we want the 
  -- edit boxes to update to reflect the new displayed event
  --CalendarCreateEventFrame.update = wrapped_update

  addon.hookdone = true
  msg("Loaded rev "..rev)
end

local function frame_OnEvent(self,event,...)
 if event == "ADDON_LOADED" then
   UpdateHooks()
 end
end

addon.frame = CreateFrame("Button", addonName.."HiddenFrame", UIParent)
addon.frame:SetScript("OnEvent", frame_OnEvent)
addon.frame:RegisterEvent("ADDON_LOADED")

