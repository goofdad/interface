local addonName, a = ...
local c = BittensGlobalTables.GetTable("BittensSpellFlashLibrary")

a.AddonName = addonName
c.Init(a)

a.SpellIDs = {
	["Avatar"] = 107574,
	["Battle Shout"] = 6673,
	["Battle Stance"] = 2457,
	["Berserker Rage"] = 18499,
	["Berserker Stance"] = 2458,
	["Bladestorm"] = 46924,
	["Bloodbath"] = 12292,
	["Bloodsurge"] = 46915,
	["Bloodthirst"] = 23881,
	["Charge"] = 100,
	["Colossus Smash"] = 86346,
	["Commanding Shout"] = 469,
	["Cleave"] = 845,
	["Defensive Stance"] = 71,
	["Demoralizing Shout"] = 1160,
	["Devestate"] = 20243,
	["Disrupting Shout"] = 102060,
	["Dragon Roar"] = 118000,
	["Enrage"] = 12880,
	["Enraged Regeneration"] = 55694,
	["Execute"] = 5308,
	["Heroic Leap"] = 6544,
	["Heroic Strike"] = 78,
	["Heroic Throw"] = 57755,
	["Incite"] = 122016,
	["Impending Victory"] = 103840,
	["Last Stand"] = 12975,
	["Meat Cleaver"] = 85739,
	["Mortal Strike"] = 12294,
	["Overpower"] = 7384,
	["Pummel"] = 6552,
	["Raging Blow"] = 85288,
	["Raging Blow!"] = 131116,
	["Rallying Cry"] = 97462,
	["Recklessness"] = 1719,
	["Revenge"] = 6572,
	["Second Wind"] = 125667,
	["Shield Barrier"] = 112048,
	["Shield Block"] = 2565,
	["Shield Slam"] = 23922,
	["Shield Wall"] = 871,
	["Shockwave"] = 46968,
	["Slam"] = 1464,
	["Spell Reflection"] = 23920,
	["Storm Bolt"] = 107570,
	["Sudden Execute"] = 139958,
	["Sunder Armor"] = 7386,
	["Sweeping Strikes"] = 12328,
	["Sword and Board"] = 50227,
	["Taste for Blood"] = 60503,
	["Taunt"] = 355,
	["Thunder Clap"] = 6343,
	["Ultimatum"] = 122510,
	["Victorious"] = 32216,
	["Victory Rush"] = 34428,
	["Wild Strike"] = 100130,
	["Whirlwind"] = 1680,
}

a.TalentIDs = {
	["Impending Victory"] = 103840,
	["Avatar"] = 107574,
	["Bloodbath"] = 12292,
}

a.GlyphIDs = {
	["Hoarse Voice"] = 58387,
	["Incite"] = 122013,
	["Shield Wall"] = 63329,
	["Victory Rush"] = 58382,
}

a.EquipmentSets = {
	DpsT14 = {
	    HeadSlot = { 86673, 85333, 87192 },
	    ShoulderSlot = { 86669, 85329, 87196 },
	    ChestSlot = { 86672, 85332, 87193 },
	    HandsSlot = { 86671, 85331, 87194 },
	    LegsSlot = { 86670, 85330, 87195 },
	},
	ProtT15 = {
	    HeadSlot = { 95993, 95337, 96737 },
	    ShoulderSlot = { 95995, 95339, 96739 },
	    ChestSlot = { 95991, 95335, 96735 },
	    HandsSlot = { 95992, 95336, 96736 },
	    LegsSlot = { 95994, 95338, 96738 },
	},
	DpsT16 = {
	    HeadSlot = { 99046, 99602, 99206, 99418 },
	    ShoulderSlot = { 99036, 99561, 99200, 99414 },
	    ChestSlot = { 99047, 99603, 99197, 99411 },
	    HandsSlot = { 99034, 99559, 99198, 99412 },
	    LegsSlot = { 99035, 99560, 99199, 99413 },
	},
}
