--[[-------------------------------------------------------------------------------
    Pets & Mounts
    Auto and random summon highly customizable for your pets and mounts, with Data Broker support.
    By: Shenton

    Localization-zhTW.lua
-------------------------------------------------------------------------------]]--

local L = LibStub("AceLocale-3.0"):NewLocale("PetsAndMounts", "zhTW");

if L then
L["10m"] = "10m" -- Needs review
L["1h"] = "1h" -- Needs review
L["1h30m"] = "1h30m" -- Needs review
L["2h"] = "2h" -- Needs review
L["30m"] = "30m" -- Needs review
L["30s"] = "30s" -- Needs review
L["3h"] = "3h" -- Needs review
L["5h"] = "5h" -- Needs review
L["Accept"] = "接受"
L["Alt"] = "Alt" -- Needs review
L["Choose"] = "選擇" -- Needs review
L["Close"] = "關閉" -- Needs review
L["Combat"] = "戰鬥" -- Needs review
L["Config frame"] = "設定視窗" -- Needs review
L["Control"] = "Control" -- Needs review
L["Data Broker"] = "Data Broker" -- Needs review
L["Death Knight"] = "死亡騎士"
L["Default"] = "預設"
L["Display the minimap icon."] = "顯示小地圖圖示."
L["Druid"] = "德魯伊"
L["Filters"] = "過濾器" -- Needs review
L["Hunter"] = "獵人"
L["Monk"] = "武僧"
L["No"] = "否" -- Needs review
L["Options"] = "選項" -- Needs review
L["Paladin"] = "聖騎士"
L["Party instance"] = "隊伍副本" -- Needs review
L["Priest"] = "牧師"
L["Raid instance"] = "團隊副本" -- Needs review
L["Rogue"] = "盜賊"
L["Select all: %s"] = "選擇全部: %s" -- Needs review
L["Selected: %s"] = "已選擇: %s" -- Needs review
L["Select Icon"] = "選擇圖示" -- Needs review
L["Shift+Click"] = "Shift+點擊" -- Needs review
L["Shimmering Moonstone"] = "幻光月石" -- Needs review
L["Text options"] = "文字選項" -- Needs review
L["Time"] = "時間" -- Needs review
L["Version"] = "版本" -- Needs review
L["Warlock"] = "術士"
L["Warrior"] = "戰士"
L["Web site"] = "網址" -- Needs review
L["Yes"] = "是" -- Needs review

end
