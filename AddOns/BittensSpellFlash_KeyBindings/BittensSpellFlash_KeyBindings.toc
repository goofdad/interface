## Interface: 50400
## Version: 50400.1.1
## Author: Xemnosyst
## Title: Bitten's SpellFlash Key Bindings
## Notes: Adds options to Blizzard's Key Bindings interface to toggle some hidden options in Bitten's SpellFlash.
## URL: http://www.curse.com/addons/wow/bittens-spellflash-key-bindings
## Dependencies: SpellFlash
## OptionalDeps: BittensSpellFlashLibrary
## X-Curse-Packaged-Version: 50400.1.1
## X-Curse-Project-Name: Bitten's SpellFlash Key Bindings
## X-Curse-Project-ID: bittens-spellflash-key-bindings
## X-Curse-Repository-ID: wow/bittens-spellflash-key-bindings/mainline
utils\BittensUtils.xml
bsl\BittensSpellFlashLibrary.xml
src\Localization.lua
src\KeyBindings.lua
