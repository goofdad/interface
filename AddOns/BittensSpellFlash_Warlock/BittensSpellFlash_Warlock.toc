## Interface: 50400
## Version: 50400.1.2
## Author: Xemnosyst
## Title: Bitten's SpellFlash: Warlock
## Notes: Replaces Blizzard's default proc highlighting to flash a maximum dps rotation.
## URL: http://www.curse.com/addons/wow/bittens-spellflash-warlock
## Dependencies: SpellFlash
## OptionalDeps: BittensSpellFlashLibrary
## LoadOnDemand: 1
## X-SpellFlashAddon-LoadWith: Warlock
## X-Curse-Packaged-Version: 50400.1.2
## X-Curse-Project-Name: Bitten's SpellFlash: Warlock
## X-Curse-Project-ID: bittens-spellflash-warlock
## X-Curse-Repository-ID: wow/bittens-spellflash-warlock/mainline
utils\BittensUtils.xml
bsl\BittensSpellFlashLibrary.xml
src\Localization.lua
src\IDs.lua
src\Spells.lua
src\Rotations.lua
src\Options.lua
