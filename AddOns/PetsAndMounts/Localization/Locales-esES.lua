--[[-------------------------------------------------------------------------------
    Pets & Mounts
    Auto and random summon highly customizable for your pets and mounts, with Data Broker support.
    By: Shenton

    Localization-esES.lua
-------------------------------------------------------------------------------]]--

local L = LibStub("AceLocale-3.0"):NewLocale("PetsAndMounts", "esES");

if L then
L["Accept"] = "Aceptar" -- Needs review
L["Activate the model rotation in the frame."] = "Activar la rotación del modelo en la ventana." -- Needs review
L["Add %s to favorite."] = "Añadir %s a favoritos." -- Needs review
L["A full database update is needed."] = "Es necesaria una actualización completa de la base de datos." -- Needs review
L["Aquatic"] = "Acuático" -- Needs review
L["A set named %s already exists, renaming it to %s."] = "El nombre de %s ya existe, renombrar a %s." -- Needs review
L["Auto summon"] = "Auto invocar" -- Needs review
L["Auto summon a random companion."] = "Auto invocar una mascota aleatoria." -- Needs review
L["Bind a key to summon a random companion."] = "Asignar una tecla para invocar una mascota al azar." -- Needs review

end
