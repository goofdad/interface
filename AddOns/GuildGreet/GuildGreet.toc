## Interface: 50400
## Title: GuildGreet-Extended
## Version: 5.4.9
## Author: gOOvER & Luzisto
## X-Category: Guild
## X-Website: http://wow.curse-gaming.com/downloads/details/7529/
## URL: http://wow.curse-gaming.com/downloads/details/7529/
## Description: Keeps a log of guildies waiting for your greeting
## Notes: Keeps a log of guildies waiting for your greeting
## SavedVariables: GLDG_Data
## X-Curse-Packaged-Version: 5.4.9
## X-Curse-Project-Name: GuildGreet Extended
## X-Curse-Project-ID: guild-greet-extended
## X-Curse-Repository-ID: wow/guild-greet-extended/mainline




Locale\locale.xml
GuildGreet.xml
