local L = LibStub("AceLocale-3.0"):NewLocale("Alts", "deDE", false)

if not L then return end

L["Add"] = "Hinzufügen"
L["Add Alt"] = "Twink hinzufügen"
L["Add Main"] = "Main hinzufügen"
L["Alt: "] = "Twink:"
L["Alt Name"] = "Twink Name"
L["Alt Names In Tooltips"] = "Twink Name im Tooltip"
L["Alts"] = "Twinks"
L["Alts: "] = "Twinks:"
L["Alts for %s: %s"] = "Twinks für %s: %s"
L["Are you sure you wish to delete the main and all user-entered alts for:"] = "Bist Du Dir sicher, das Du den Main und alle selbsthinzugefügten Twinks löschen willst für:" -- Needs review
L["Are you sure you wish to remove"] = "Bist Du Dir sicher, das Du das entfernen willst" -- Needs review
L["Auto Import Guild"] = "Auto Import Gilde" -- Needs review
L["Cancel"] = "Abbrechen" -- Needs review
L["Close"] = "Schliessen" -- Needs review
L["Delete"] = "Löschen" -- Needs review
L["Delete Alt"] = "Twink löschen" -- Needs review
L["Delete Main"] = "Main löschen" -- Needs review
L["Main: "] = "Main:" -- Needs review
L["Minimap Button"] = true -- Needs review
L["No alts found for "] = "Keine Twinks gefunden für" -- Needs review
L["No main found for "] = "Kein Main gefunden für" -- Needs review

