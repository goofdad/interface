﻿## Interface: 50400
## Title: RareAnnouncer
## Version: 5.4.1.3A
## RareAnnouncer Author: Tondef, Drag0nsAng3l
## Branch Author: Tondef, Drag0nsAng3l
## Notes: Sends spawn/kill events to general channel (optional) and background synchronization channel to call other players to help kill rare NPCs on the Timeless Isle.
## URL: http://wow.curseforge.com/addons/RareAnnouncer.html
## URL: http://www.curse.com/addons/wow/RareAnnouncer
## SavedVariables: RAREAN
## DefaultState: enabled
## X-Category: Chat, Achievements, Quests
## X-Date: 09/28/2013
## X-Localizations: enUS, enGB, esES. zhTW, deDE, ruRU
## X-CompatibleLocales: enUS, enGB, esES, zhTW, deDE, ruRU
## X-Curse-Packaged-Version: 5.4.1.6
## X-Curse-Project-Name: RareAnnouncer
## X-Curse-Project-ID: rareannouncer
## X-Curse-Repository-ID: wow/rareannouncer/mainline

AddonCore.lua

Localization.enUS.lua
Localization.esES.lua
Localization.zhTW.lua
Localization.deDE.lua
Localization.ruRU.lua
RareAnnouncer.lua