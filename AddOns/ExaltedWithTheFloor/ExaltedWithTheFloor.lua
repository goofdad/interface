ExaltedWithFloor = LibStub("AceAddon-3.0"):NewAddon("Exalted with the Floor", "AceEvent-3.0", "AceConsole-3.0", "AceComm-3.0", "AceTimer-3.0")

AceGUI = LibStub("AceGUI-3.0")

local FACTION_BAR_COLOURS = {
    [1] = {r = 0.9, g = 0.7, b = 0},
    [2] = {r = 0, g = 0.6, b = 0.1},
    [3] = {r = 0, g = 0.6, b = 0.1},
    [4] = {r = 0, g = 0.6, b = 0.1},
    [5] = {r = 0, g = 0.6, b = 0.1},
};

local NUM_OF_WORLD_SUB_SECTIONS = 5
local MSG_PREFIX_SEND = "RWFSTP"
local MSG_PREFIX_TOOLTIP = "RWFTP"
local DEATH_COUNT = 0
local lineAdded = false

local defaults = {
  char = {
	version = 1.0,
	floorReps = {
		{name = "Floor", desc = "", barMin = 0, barMax = 0, barValue = 0, isHeader = true, hasRep = false, isChild = false, isCollapsed = false}, 
		{name = "Dungeon", desc = "Floor reputation gained while in Dungeons", barMin = 0, barMax = 3000, barValue = 0, isHeader = false, hasRep = true, isChild = true, isCollapsed = false},
		{name = "Scenario", desc = "Floor reputation gained while in a Scenario", barMin = 0, barMax = 3000, barValue = 0, isHeader = false, hasRep = true, isChild = true, isCollapsed = false},
		{name = "Arena", desc = "Floor reputation gained while in a Arena", barMin = 0, barMax = 3000, barValue = 0, isHeader = false, hasRep = true, isChild = true, isCollapsed = false},
		{name = "Brawler's Guild", desc = "Floor reputation gained while in the Brawler's Guild", barMin = 0, barMax = 3000, barValue = 0, isHeader = false, hasRep = true, isChild = true, isCollapsed = false},
		
		{name = "World Floors", desc = "", barMin = 0, barMax = 3000, barValue = 0, isHeader = true, hasRep = true, isChild = false, isCollapsed = false},
		{name = "Eastern Kingdoms", desc = "Floor reputation gained while in Eastern Kingdoms", barMin = 0, barMax = 3000, barValue = 0, isHeader = false, hasRep = true, isChild = true, isCollapsed = false},
		{name = "Kalimdor", desc = "Floor reputation gained while in Kalimdor", barMin = 0, barMax = 3000, barValue = 0, isHeader = false, hasRep = true, isChild = true, isCollapsed = false},
		{name = "Outlands", desc = "Floor reputation gained while in Outlands", barMin = 0, barMax = 3000, barValue = 0, isHeader = false, hasRep = true, isChild = true, isCollapsed = false},
		{name = "Northrend", desc = "Floor reputation gained while in Northrend", barMin = 0, barMax = 3000, barValue = 0, isHeader = false, hasRep = true, isChild = true, isCollapsed = false},
		{name = "Pandaria", desc = "Floor reputation gained while in Pandaria", barMin = 0, barMax = 3000, barValue = 0, isHeader = false, hasRep = true, isChild = true, isCollapsed = false}, 
		
		{name = "Raid Floors", desc = "", barMin = 0, barMax = 0, barValue = 0, isHeader = true, hasRep = false, isChild = false, isCollapsed = false}, 
		{name = "Siege of Orgrimmar", desc = "Floor reputation gained while in the Siege of Orgrimmer", barMin = 0, barMax = 3000, barValue = 0, isHeader = false, hasRep = true, isChild = true, isCollapsed = false}, 
		{name = "Throne of Thunder", desc = "Floor reputation gained while in the Throne of Thunder", barMin = 0, barMax = 3000, barValue = 0, isHeader = false, hasRep = true, isChild = true, isCollapsed = false}, 
		{name = "Terrace of Endless Springs", desc = "Floor reputation gained while in the Terrace of Endless Springs", barMin = 0, barMax = 3000, barValue = 0, isHeader = false, hasRep = true, isChild = true, isCollapsed = false}, 
		{name = "Heart of Fear", desc = "Floor reputation gained while in the Heart of Fear", barMin = 0, barMax = 3000, barValue = 0, isHeader = false, hasRep = true, isChild = true, isCollapsed = false}, 
		{name = "Mogu'shan Palace", desc = "Floor reputation gained while in the Mogu'shan Palace", barMin = 0, barMax = 3000, barValue = 0, isHeader = false, hasRep = true, isChild = true, isCollapsed = false}, 
		{name = "Previous Raid", desc = "Floor reputation gained while in raids pre Mists of Pandaria", barMin = 0, barMax = 3000, barValue = 0, isHeader = false, hasRep = true, isChild = true, isCollapsed = false},
		
		{name = "Battleground Floors", desc = "", barMin = 0, barMax = 0, barValue = 0, isHeader = true, hasRep = false, isChild = false, isCollapsed = false},
		{name = "Arathi Basin", desc = "", barMin = 0, barMax = 3000, barValue = 0, isHeader = false, hasRep = true, isChild = true, isCollapsed = false},
		{name = "Warsong Gulch", desc = "", barMin = 0, barMax = 3000, barValue = 0, isHeader = false, hasRep = true, isChild = true, isCollapsed = false},
		{name = "Eye of the Storm", desc = "", barMin = 0, barMax = 3000, barValue = 0, isHeader = false, hasRep = true, isChild = true, isCollapsed = false},
		{name = "Alterac Valley", desc = "", barMin = 0, barMax = 3000, barValue = 0, isHeader = false, hasRep = true, isChild = true, isCollapsed = false},
		{name = "Strand of the Ancients", desc = "", barMin = 0, barMax = 3000, barValue = 0, isHeader = false, hasRep = true, isChild = true, isCollapsed = false},
		{name = "Isle of Conquest", desc = "", barMin = 0, barMax = 3000, barValue = 0, isHeader = false, hasRep = true, isChild = true, isCollapsed = false},
		{name = "Twin Peaks", desc = "", barMin = 0, barMax = 3000, barValue = 0, isHeader = false, hasRep = true, isChild = true, isCollapsed = false},
		{name = "The Battle for Gilneas", desc = "", barMin = 0, barMax = 3000, barValue = 0, isHeader = false, hasRep = true, isChild = true, isCollapsed = false},
		{name = "Temple of Kotmogu", desc = "", barMin = 0, barMax = 3000, barValue = 0, isHeader = false, hasRep = true, isChild = true, isCollapsed = false},
		{name = "Silvershard Mines", desc = "", barMin = 0, barMax = 3000, barValue = 0, isHeader = false, hasRep = true, isChild = true, isCollapsed = false},
		{name = "Deepwind Gorge", desc = "", barMin = 0, barMax = 3000, barValue = 0, isHeader = false, hasRep = true, isChild = true, isCollapsed = false},
		},
	}
}

local VERSION = GetAddOnMetadata("ExaltedWithFloor", "Version")

function ExaltedWithFloor:OnInitialize()
	self.db = LibStub("AceDB-3.0"):New("ExaltedWithFloorDB", defaults, true)
	ExaltedWithFloor:RegisterEvent("PLAYER_DEAD", RWFDeath)
	ExaltedWithFloor:RegisterEvent("UPDATE_MOUSEOVER_UNIT", ExaltedWithFloor_ToolTip) 
	if (VERSION ~= self.db.char.version) then
		ExaltedWithFloor:UpdateProfile()
	end
end

function ExaltedWithFloor:OnEnable()
	original_RWF_ReputationFrame_OnShow = ReputationFrame_OnShow
	ExaltedWithFloor:RegisterComm(MSG_PREFIX_SEND)
	ExaltedWithFloor:RegisterComm(MSG_PREFIX_TOOLTIP)
end

function ExaltedWithFloor_ToolTip()
	local name, faction = UnitName("mouseover")
	if (UnitExists("mouseover") == 1 and not UnitAffectingCombat("player") and name ~= "Unknown" ) then
		if (UnitIsPlayer("mouseover") == 1 and UnitIsFriend("player", "mouseover")) then
			local name, type, _, difficulty = GetInstanceInfo()
			for id, value in pairs(ExaltedWithFloor.db.char.floorReps) do
				if ( value.name == FloorType(name, type, difficulty)) then
					local sendMessage = value.name.."."..id
					local sendTo = GetUnitName("mouseover", true)
					ExaltedWithFloor:SendCommMessage(MSG_PREFIX_SEND, sendMessage, "WHISPER", sendTo)
					break
				end
			end
		end
	end
end

function ExaltedWithFloor:OnCommReceived(prefix, message, distribution, sender)
	local playerName = UnitName("player")
	if (prefix == MSG_PREFIX_SEND) then
		local name, id = strsplit(".", message)
		local standing, _ = GetFloorStandingText(tonumber(id))
		local current, max

		for id, value in pairs(ExaltedWithFloor.db.char.floorReps) do
			if ( value.name == name) then
				current = value.barValue
				max = value.barMax
				break
			end
		end

		if( name ~= nil and standing ~= nil and current ~= nil and max ~= nil) then
			local sendMessage = name..": "..standing.." ("..current.."/"..max..")"
			ExaltedWithFloor:SendCommMessage(MSG_PREFIX_TOOLTIP, sendMessage, "WHISPER", sender)
		end
	end
	if (prefix == MSG_PREFIX_TOOLTIP and sender == GetUnitName("mouseover", true) and lineAdded == false) then
		lineAdded = true
		GameTooltip:AddLine(message, 1,1,1,1)    
		GameTooltip:Show()
	end
end

local function OnTooltipCleared(tooltip, ...)
   lineAdded = false
end

GameTooltip:HookScript("OnTooltipCleared", OnTooltipCleared)

function tableLength(T) 
	local c = 0 
	for _, v in pairs(T) do 
		if (v.isCollapsed == true and v.isChild == true) then
		else
			c = c + 1
		end 
	end 
	return c 
end

function RealTableLength(T) 
	local b = 0 
	for _, v in pairs(T) do b = b + 1 end
	return b
end

ReputationFrame:HookScript("OnShow", function(frame)
	RWF_ReputationDetailButton:Show()
	original_RWF_ReputationFrame_OnShow()
	end)

function RWF_Update()
	local numFactions = tableLength(ExaltedWithFloor.db.char.floorReps)

	if ( not FauxScrollFrame_Update(RWF_ReputationListScrollFrame, numFactions, 15, 26 ) ) then
        RWF_ReputationListScrollFrameScrollBar:SetValue(0);
    end

    local factionOffset = FauxScrollFrame_GetOffset(RWF_ReputationListScrollFrame);

	for i=1, 15, 1 do
		local factionIndex = factionOffset + i;
        local factionRow = _G["RWF_ReputationBar"..i];
        local factionBar = _G["RWF_ReputationBar"..i.."ReputationBar"];
        local factionTitle = _G["RWF_ReputationBar"..i.."FactionName"];
        local factionButton = _G["RWF_ReputationBar"..i.."ExpandOrCollapseButton"];
        local factionStanding = _G["RWF_ReputationBar"..i.."ReputationBarFactionStanding"];
        local factionBackground = _G["RWF_ReputationBar"..i.."Background"];

        if ( factionIndex <= numFactions ) then
			local name, description, barMin, barMax, barValue, isHeader, hasRep, isChild, isCollapsed = GetFloorInfo(factionIndex);
			factionTitle:SetText(name);

			if ( isCollapsed ) then
                factionButton:SetNormalTexture("Interface\\Buttons\\UI-PlusButton-Up");
            else
                factionButton:SetNormalTexture("Interface\\Buttons\\UI-MinusButton-Up"); 
            end

            factionRow.index = factionIndex;
            factionRow.isCollapsed = isCollapsed;
			local factionStandingText, standingID = GetFloorStandingText(factionIndex)

			factionStanding:SetText(factionStandingText);

			--Normalize Values
            barMax = barMax - barMin;
            barValue = barValue - barMin;
            barMin = 0;

            factionRow.standingText = factionStandingText;
			factionBar:SetMinMaxValues(0, barMax);
            factionBar:SetValue(barValue);
			local barColour = FACTION_BAR_COLOURS[standingID];
			factionBar:SetStatusBarColor(barColour.r, barColour.g, barColour.b);
			factionRow.tooltip = barValue.." / "..barMax;
			RWF_ReputationFrame_SetRowType(factionRow, isChild, isHeader, hasRep);

            factionRow:Show();
		else
            factionRow:Hide();
		end
	end
end

function RWF_ReputationFrame_SetRowType(factionRow, isChild, isHeader, hasRep)  --rowType is a binary table of type isHeader, isChild
    local factionRowName = factionRow:GetName()
    local factionBar = _G[factionRowName.."ReputationBar"];
    local factionTitle = _G[factionRowName.."FactionName"];
    local factionStanding = _G[factionRowName.."ReputationBarFactionStanding"];
    local factionBackground = _G[factionRowName.."Background"];
    local factionButton = _G[factionRowName.."ExpandOrCollapseButton"];
    local factionLeftTexture = _G[factionRowName.."ReputationBarLeftTexture"];
    local factionRightTexture = _G[factionRowName.."ReputationBarRightTexture"];

    factionLeftTexture:SetWidth(62);
    factionRightTexture:SetWidth(42);
    factionBar:SetPoint("RIGHT", factionRow, "RIGHT", 0, 0);

    if ( isHeader ) then
        if ( isChild ) then
            factionRow:SetPoint("LEFT", RWF_SubFrame, "LEFT", 29, 0);
            factionTitle:SetWidth(135);
        else
            factionRow:SetPoint("LEFT", RWF_SubFrame, "LEFT", 0, 0);
            factionTitle:SetWidth(145);
        end
		factionButton:SetPoint("LEFT", factionRow, "LEFT", 3, 0);
        factionButton:Show();
		factionTitle:SetPoint("LEFT",factionButton,"RIGHT",10,0);
        factionTitle:SetFontObject(GameFontNormalLeft);
        factionBackground:Hide()    
        factionLeftTexture:SetHeight(15);
        factionLeftTexture:SetWidth(60);
        factionRightTexture:SetHeight(15);
        factionRightTexture:SetWidth(39);
        factionLeftTexture:SetTexCoord(0.765625, 1.0, 0.046875, 0.28125);
        factionRightTexture:SetTexCoord(0.0, 0.15234375, 0.390625, 0.625);
        factionBar:SetWidth(99);
    else
        if ( isChild ) then
            factionRow:SetPoint("LEFT", RWF_SubFrame, "LEFT", 25, 0);
            factionTitle:SetWidth(150);
        else
            factionRow:SetPoint("LEFT", RWF_SubFrame, "LEFT", 30, 0);
            factionTitle:SetWidth(160);
        end
		factionButton:Hide();
        factionTitle:SetPoint("LEFT", factionRow, "LEFT", 10, 0);
        factionTitle:SetFontObject(GameFontHighlightSmall);
        factionBackground:Show();
        factionLeftTexture:SetHeight(21);
        factionRightTexture:SetHeight(21);
        factionLeftTexture:SetTexCoord(0.7578125, 1.0, 0.0, 0.328125);
        factionRightTexture:SetTexCoord(0.0, 0.1640625, 0.34375, 0.671875);
        factionBar:SetWidth(101)
    end

    if ( (hasRep) or (not isHeader) ) then
        factionStanding:Show();
        factionBar:Show();
        factionBar:GetParent().hasRep = true;
    else
        factionStanding:Hide();
        factionBar:Hide();
        factionBar:GetParent().hasRep = false;
    end
end


function GetFloorInfo(index)
	local name, description, barMin, barMax, barValue, isHeader, hasRep, isChild, isCollapsed
	for i, value in pairs(ExaltedWithFloor.db.char.floorReps) do
		if (i == index and value.isChild == false) then
			name = value.name
			description = value.desc
			barMin = value.barMin
			barMax = value.barMax
			barValue = value.barValue
			isHeader = value.isHeader
			hasRep = value.hasRep
			isChild = value.isChild
			isCollapsed = value.isCollapsed
			
		elseif (i == index and (value.isChild == true and value.isCollapsed == false)) then
			name = value.name
			description = value.desc
			barMin = value.barMin
			barMax = value.barMax
			barValue = value.barValue
			isHeader = value.isHeader
			hasRep = value.hasRep
			isChild = value.isChild
			isCollapsed = value.isCollapsed
		elseif (value.isCollapsed == true and value.isChild == true) then
			index = index + 1
		end
	end
	return name, description, barMin, barMax, barValue, isHeader, hasRep, isChild, isCollapsed
end

function GetFloorStandingText(index)
	local standing = ""
	local id = 1
	for i, value in pairs(ExaltedWithFloor.db.char.floorReps) do
		if (i == index) then
			--Check if it is Unfriendly or Below first
			if (value.barMax == 3000) then
				standing = "Neutral"
				id = 1
				break
			elseif (value.barMax == 6000) then
				standing = "Friendly"
				id = 2
				break
			elseif (value.barMax == 12000) then
				standing = "Honored"
				id = 3
				break
			elseif (value.barMax == 21000) then
				standing = "Revered"
				id = 4
				break
			else
				standing = "Exhalted"
				id = 5
				break
			end
		end
	end
	return standing, id
end

function RWFDeath()
	if (DEATH_COUNT == 0) then
		ExaltedWithFloor.timer = ExaltedWithFloor:ScheduleTimer(AllowDeath, 1);
		DEATH_COUNT = 1
		local name, type, _, difficulty = GetInstanceInfo()
		if ( type == "none" and (difficulty == "" or difficulty == 0) and name ~="Deeprun Tram" and name ~="Brawl'gar Arena") then
			AwardFloorRep("World Floors")
		end
		AwardFloorRep(FloorType(name, type, difficulty))
	end
end

function AllowDeath()
	DEATH_COUNT = 0
end

function FloorType(name, type, difficulty)
	local floorType
	if (name == "Deeprun Tram" or name == "Brawl'gar Arena") then
		floorType = "Brawler's Guild"
	elseif (type == "raid") then
		for _, v in pairs(ExaltedWithFloor.db.char.floorReps) do
			if (v.name == name) then
				floorType = name
				break
			end
		end
		if (floorType == nil) then
			floorType = "Previous Raid"
		end
	elseif (type == "pvp") then
		for _, v in pairs(ExaltedWithFloor.db.char.floorReps) do
			if (v.name == name) then
				floorType = name
				break
			end
		end
	elseif (type == "party") then
		floorType = "Dungeon"
	elseif (type == "arena") then
		floorType = "Arena"
	elseif (type == "scenario") then
		floorType = "Scenario"
	elseif (type == "none" and (difficulty == "" or difficulty == 0)) then 
		floorType = RWF_WhereInTheWorld(GetCurrentMapContinent())
	else
		ExaltedWithFloor:Print("Error Occured: Name: "..name.." Type: "..type.." Difficulty "..difficulty)
		floorType = ""
	end

	return floorType
end

function AwardFloorRep(type)
	if ( type ~= "1" ) then
		local standing = ""
		if (type == "Previous Raid") then
			DEFAULT_CHAT_FRAME:AddMessage("Reputation with a "..type.." floor increased by 100.", 0.5,0.5,0.9)
		elseif (type == "Scenario") then
			DEFAULT_CHAT_FRAME:AddMessage("Reputation with the Scenario floor increased by 100.", 0.5,0.5,0.9)
		elseif (type == "World Floors") then
			DEFAULT_CHAT_FRAME:AddMessage("Reputation with the World floor increased by "..(100 / NUM_OF_WORLD_SUB_SECTIONS)..".", 0.5,0.5,0.9)
		else
			DEFAULT_CHAT_FRAME:AddMessage("Reputation with the "..type.." floor increased by 100.", 0.5,0.5,0.9)
		end

		for _, value in pairs(ExaltedWithFloor.db.char.floorReps) do
			if ( value.name == type) then
				if (type == "World Floors") then
					value.barValue = value.barValue + (100 / NUM_OF_WORLD_SUB_SECTIONS)
				else
					value.barValue = value.barValue + 100
				end
				if ( value.barValue > value.barMax )  then
					value.barValue = value.barValue - value.barMax
				
					if (value.barMax == 3000) then
						standing = "Friendly"
						value.barMax = 6000
					elseif (value.barMax == 6000) then
						standing = "Honored"
						value.barMax = 12000						
					elseif (value.barMax == 12000) then
						standing = "Revered"
						value.barMax = 21000						
					elseif (value.barMax == 21000) then
						standing = "Exhalted"
						value.barMax = 999						
					else
						value.barValue = 999
						break						
					end
					local Msg
					if (type == "Previous Raid") then
						Msg = (UnitName("player").." has become "..standing.." with a "..type.." floor.")
					elseif (type == nil) then
						Msg = (UnitName("player").." has gained 100 reputation with the Scenario floor.")
					elseif (type == "World Floors") then
						Msg = (UnitName("player").." has become "..standing.." with the World floor.")
					else
						Msg = (UnitName("player").." has become "..standing.." with the "..type.." floor.")
					end
					print(Msg)
					SendChatMessage(Msg, "GUILD")
					break
				end
			end
		end

		RWF_Update();
	end
end

function RWF_ExpandFactionHeader(id)
	local numFactions = RealTableLength(ExaltedWithFloor.db.char.floorReps)
	if (id < 15) then 
		local factionRowName = _G["RWF_ReputationBar"..id.."FactionName"];
		local name = factionRowName:GetText()
		for x, V in pairs(ExaltedWithFloor.db.char.floorReps) do
			if (V.name == name) then
				id = x
			end
		end
	end

	ExaltedWithFloor.db.char.floorReps[id].isCollapsed = false

	for i = id + 1, numFactions, 1 do
		if ( ExaltedWithFloor.db.char.floorReps[i].isChild == false ) then
			break
		else
			ExaltedWithFloor.db.char.floorReps[i].isCollapsed = false
		end
	end
	RWF_Update();
end

function RWF_CollapseFactionHeader(id)
	local numFactions = RealTableLength(ExaltedWithFloor.db.char.floorReps)
	if (id < 16) then 
		local factionRowName = _G["RWF_ReputationBar"..id.."FactionName"];
		local name = factionRowName:GetText()
		for x, V in pairs(ExaltedWithFloor.db.char.floorReps) do
			if (V.name == name) then
				id = x
			end
		end
	end

	ExaltedWithFloor.db.char.floorReps[id].isCollapsed = true

	for i = id + 1, numFactions, 1 do
		if ( ExaltedWithFloor.db.char.floorReps[i].isChild == false ) then
			break
		else
			ExaltedWithFloor.db.char.floorReps[i].isCollapsed = true
		end
	end
	RWF_Update();
end

function RWF_WhereInTheWorld(area)
	if (area == -1) then
		return "1"
	elseif (area == 0) then
		return "1"
	elseif (area == 1) then
		return "Kalimdor"
	elseif (area == 2) then
		return "Eastern Kingdoms"
	elseif (area == 3) then
		return "Outland"
	elseif (area == 4) then
		return "Northrend"
	elseif (area == 5) then
		return "1"
	elseif (area == 6) then
		return "Pandaria"
	end
end