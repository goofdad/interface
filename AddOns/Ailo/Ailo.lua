-- Helperfunctions
local date, time, select, pairs, ipairs, type, next = date, time, select, pairs, ipairs, type, next
local strlower, strgsub, strformat, tostring = string.lower, string.gsub, string.format, tostring
local _, wipe, tsort, tinsert = _, wipe, table.sort, table.insert
local floor, fmod, hugeNumber, max, min = math.floor, math.fmod, math.huge, math.max, math.min
local GetRemainingCompletions = LFGRewardsFrame_EstimateRemainingCompletions

-- LibDataBroker
local ldb = LibStub:GetLibrary("LibDataBroker-1.1", true)
local LDBIcon = ldb and LibStub("LibDBIcon-1.0",true)
local LibQTip = LibStub('LibQTip-1.0')
local Ailo = LibStub("AceAddon-3.0"):NewAddon("Ailo", "AceConsole-3.0", "AceEvent-3.0", "AceHook-3.0", "AceTimer-3.0")
local L = LibStub("AceLocale-3.0"):GetLocale("Ailo", false)
local DB_VERSION = 13
local currentChar, currentRealm, currentCharRealm
local icons = setmetatable( {}, { __index = function() return "Interface\\Icons\\INV_Misc_QuestionMark" end } )

-- updateRetry variables used by FirstUpdate timer callback
local updateRetryLimit = 10
local updateRetryCount = 0
local updateRetryTimer = nil
local updateLockoutTimer = nil

function Ailo:Output(...)
	if self.db.profile.showMessages then
		Ailo:Print(...)
	end
end 

-- Sorting
local sortRealms = {}
local sortRealmsPlayer = {}

local RAID_CLASS_COLORS_FONTS = {}

local function setColor(info, r, g, b, a)
	Ailo.db.profile[info[#info]] = { r = r, g = g, b = b, a = a }
end

local function getColor(info)
	return Ailo.db.profile[info[#info]].r, Ailo.db.profile[info[#info]].g, Ailo.db.profile[info[#info]].b, Ailo.db.profile[info[#info]].a
end

local function GetWeeklyResetDay()
	-- number is index into day of week; 1 == Sunday
	local resets = { 
		enCN = 3, enUS = 3, esMX = 3, ptBR = 3,                                   -- Tuesday 
		deDE = 4, enGB = 4, esES = 4, frFR = 4, itIT = 4, ptPT = 4, ruRU = 4,     -- Wednesday
		enTW = 5, koKR = 5, zhCN = 5, zhTW = 5,                                   -- Thursday
	}

	local locale = GetLocale()
	local reset = resets[locale]

	if not reset then Ailo:Print(locale, "locale is not recognized, please report this error") end
	return reset or 3
end

local function GetNextDailyReset()
	-- when first logging in, we may get a bogus result from GetQuestResetTime
	-- if thats the case, then just return hugeNumber so that nothing gets purged
	local qrtime = GetQuestResetTime()
	if qrtime <= 0 then
		return hugeNumber
	end

	return time() + qrtime
end

local function GetNextWeeklyReset()
	-- if we're not getting a valid number for the daily reset, then we can't
	-- calculate the weekly. just return hugeNumber so that nothing gets purged 
	local nextDailyReset = GetNextDailyReset()
	if (nextDailyReset == hugeNumber) then
		return hugeNumber
	end

	local dayOfNextReset = date("*t", nextDailyReset).wday
	local daysToWeeklyReset = fmod(7 + Ailo.db.global.weeklyResetDay - dayOfNextReset, 7)
	local nextWeeklyReset = nextDailyReset + (daysToWeeklyReset * 86400) -- 86400 is number of seconds in a day (24 * 60 * 60)

	return nextWeeklyReset
end


--=========================================================================--
-- data table definitions
--=========================================================================--

local defaults = {
	profile = {
		savedraid = { r=1, g=0, b=0, a=1 },
		freeraid  = { r=0, g=1, b=0, a=1 },
		hc  = "hc",
		nhc = "nhc",
		show5Man                = false,
		showAllChars            = false,
		showCharacterRealm      = false,
		showWeeklyValor         = true,
		showDailyHeroicDungeon  = true,
		showDailyHeroicScenario = false,
		showDailyScenario       = false,
		showLeft                = false,
		showMessages            = true,
		showRealmHeaderLines    = false,
		showFlexRaid            = true,
		showRaidFinder          = true,
		showWorldBosses         = true,
		showTBVictory           = false,
		showWGVictory           = false,
		showWeeklyConquest      = false,
		showDailyPVP            = false,
		showSeasonal            = true,
		showTillers             = true,
		showLootCoins           = true,
		useClassColors          = true,
		useCustomClassColors    = true,
		minLevel = MAX_PLAYER_LEVEL_TABLE[GetExpansionLevel()],
		instanceAbbr = {},
		minimapIcon = {
			hide = false,
			minimapPos = 220,
			radius = 80,
		},
	},
	global = {
		chars = {},             -- structure is = { realm = { char = { lockouts = { expire, encounter }, resets = { expire, encounter } } } }  
		charClass = {},
		raids = {},             -- structure is = { instance = { size = true } }
		lfrs  = {},             -- structure is = { id = expire }
		world = {},             -- structure is = { name = expire }
		nextPurge = hugeNumber,
		weeklyResetDay = 3,     -- TODO - set default based on region
		version = 0,
	},
}

local Seasonal = {}
Seasonal.Events = {
    Valentines = { icon = "Interface\\Icons\\inv_valentinesboxofchocolates02", 
                  first = time( { year = 2014, month = 2,  day = 10, hour = 11, min = 0 } ),
                   last = time( { year = 2014, month = 2,  day = 24, hour = 11, min = 0 } ),
                  level = 1,
             dungeon_id = 288 },
     Midsummer = { icon = "Interface\\Icons\\inv_misc_bag_17", 
                  first = time( { year = 2013, month = 6,  day = 21, hour = 11, min = 0 } ),
                   last = time( { year = 2013, month = 7,  day =  5, hour = 11, min = 0 } ),
                  level = 1,
             dungeon_id = 286 },
      Brewfest = { icon = "Interface\\Icons\\inv_cask_02", 
                  first = time( { year = 2013, month = 9,  day = 20, hour = 11, min = 0 } ),
                   last = time( { year = 2013, month = 10, day =  5, hour = 11, min = 0 } ),
                  level = 1,
             dungeon_id = 287 },
    HallowsEnd = { icon = "Interface\\Icons\\inv_misc_bag_28_halloween", 
                  first = time( { year = 2013, month = 10, day = 18, hour = 11, min = 0 } ),
                   last = time( { year = 2013, month = 11, day =  1, hour = 12, min = 0 } ),
                  level = 1,
             dungeon_id = 285 },
    WinterVeil = { icon = "Interface\\Icons\\inv_holiday_christmas_present_01",
                  first = time( { year = 2013, month = 12, day = 16, hour = 11, min = 0 } ),
                   last = time( { year = 2014, month = 1,  day = 2,  hour = 7,  min = 0  } ),
                  level = 0,
              quest_ids = { 6983, 7043 }, },
}

local RaidFinder = { 
	-- Siege of Orgrimmar, tier 16, Mists of Pandaria
	{ id = 730, count = 3, GetExpireTime = GetNextWeeklyReset, flex = true }, -- Downfall
	{ id = 729, count = 3, GetExpireTime = GetNextWeeklyReset, flex = true }, -- The Underhold
	{ id = 728, count = 4, GetExpireTime = GetNextWeeklyReset, flex = true }, -- Gates of Retribution
	{ id = 726, count = 4, GetExpireTime = GetNextWeeklyReset, flex = true }, -- Vale of Eternal Sorrows
	{ id = 725, count = 3, GetExpireTime = GetNextWeeklyReset }, -- Downfall
	{ id = 724, count = 3, GetExpireTime = GetNextWeeklyReset }, -- The Underhold
	{ id = 717, count = 4, GetExpireTime = GetNextWeeklyReset }, -- Gates of Retribution
	{ id = 716, count = 4, GetExpireTime = GetNextWeeklyReset }, -- Vale of Eternal Sorrows

	-- Throne of Thunder, tier 15, Mists of Pandaria
	{ id = 613, count = 3, GetExpireTime = GetNextWeeklyReset }, -- Pinnacle of Storms
	{ id = 612, count = 3, GetExpireTime = GetNextWeeklyReset }, -- Halls of Flesh-Shaping
	{ id = 611, count = 3, GetExpireTime = GetNextWeeklyReset }, -- Forgotten Depths
	{ id = 610, count = 3, GetExpireTime = GetNextWeeklyReset }, -- Last Stand of the Zandalari

	-- Terrace of Endless Spring, tier 14, Mists of Pandaria
	{ id = 526, count = 4, GetExpireTime = GetNextWeeklyReset }, -- Terrace of Endless Spring
	-- Heart of Fear, tier 14, Mists of Pandaria
	{ id = 529, count = 3, GetExpireTime = GetNextWeeklyReset }, -- The Dread Approach
	{ id = 530, count = 3, GetExpireTime = GetNextWeeklyReset }, -- Nightmare of Shek'zeer
	-- Mogu'shan Vaults, non-tier, Mists of Pandaria
	{ id = 527, count = 3, GetExpireTime = GetNextWeeklyReset }, -- Guardians of Mogu'shan
	{ id = 528, count = 3, GetExpireTime = GetNextWeeklyReset }, -- The Vault of Mysteries

	-- Dragonsoul, tier 13, Cataclysm
	{ id = 416, count = 4, GetExpireTime = GetNextWeeklyReset }, -- The Siege of Wyrmrest Temple
	{ id = 417, count = 4, GetExpireTime = GetNextWeeklyReset }, -- Fall of Deathwing
}

local RepeatableQuests = {
	-- ID's of horde and alliance versions of Victory in Wintergrasp weekly pvp quest 
	WGVictory = { GetExpireTime = GetNextWeeklyReset, questIds = { 13181, 13183 } },
	-- ID's of horde and alliance versions of the Victory in Tol Barad weekly quest 
	TBVictory = { GetExpireTime = GetNextWeeklyReset, questIds = { 28882, 28884 } },
	-- ID's of all horde and alliance versions of repeatable Seasonal quests
	-- Note: supports only one h/a set of quests per event, but all events can live
	-- in same struct as long as they don't have overlapping run times.
	Seasonal = { GetExpireTime = GetNextDailyReset, questIds = { 6983, 7043 } },
	-- ID's of weekly quest to gain 3 LootCoins 
	LootCoins = { GetExpireTime = GetNextWeeklyReset, questIds = { 33133, 33134 } },
}

local Currencies = { -- table will also cache current and max values for total and weekly
	Valor     = { id = _G.VALOR_CURRENCY,    scaler = .01 },
	Conquest  = { id = _G.CONQUEST_CURRENCY, scaler = 1   },
	LootCoins = { id = 776,                  scaler = 1   }, -- 776 = warforged seals
}

--=========================================================================--
--=========================================================================--
--
-- Addon Initialization
--
--=========================================================================--
--=========================================================================--

function Ailo:OnInitialize() -- ADDON_LOADED, only saved variables are guaranteed to be present
	defaults.global.weeklyResetDay = GetWeeklyResetDay()
	self.db = LibStub("AceDB-3.0"):New("AiloDB", defaults, true)
	self:MigrateSavedData()

	LibStub("AceConfig-3.0"):RegisterOptionsTable("Ailo", self.GenerateOptions)
	self.optionsFrame = LibStub("AceConfigDialog-3.0"):AddToBlizOptions("Ailo")

	local AiloLDB = ldb:NewDataObject("Ailo", {
		type = "data source",
		text = "Ailo",
		icon = "Interface\\Icons\\Achievement_Dungeon_UlduarRaid_Archway_01.png",
		OnClick = function(clickedframe, button)
			if button == "RightButton" then 
				InterfaceOptionsFrame_OpenToCategory(Ailo.optionsFrame) 
			else 
				if IsShiftKeyDown() then
					Ailo:ManualPlayerUpdate() 
				else
					ToggleFriendsFrame(4)
				end
			end
		end,
		OnEnter = function(tt)
			local tooltip = LibQTip:Acquire("AiloTooltip", 1, "LEFT") 
			Ailo.tooltip = tooltip
			Ailo:PrepareTooltip(tooltip) 
			tooltip:SmartAnchorTo(tt)
			tooltip:Show()
		end,
		OnLeave = function(tt)
			LibQTip:Release(Ailo.tooltip)
			Ailo.tooltip = nil
		end,
	})

	LDBIcon:Register("Ailo", AiloLDB, self.db.profile.minimapIcon)
end

function Ailo:OnEnable() -- PLAYER_LOGIN, game data is initialized
	currentChar = UnitName("player")
	currentRealm = GetRealmName()
	currentCharRealm = currentChar..' - '..currentRealm

	self:InitIconsTable()
	self:InitCurrenciesTable()

	self:SetupClasscoloredFonts()
	if CUSTOM_CLASS_COLORS then
		CUSTOM_CLASS_COLORS:RegisterCallback("SetupClasscoloredFonts", self)
	end

	if self.db.profile.minLevel <= UnitLevel("player") then 
		updateRetryTimer = self:ScheduleRepeatingTimer("DelayedInit", .2)
	elseif self.db.global.chars[currentRealm] then
		-- This character was once elligible to be shown and so would still be shown if 
		-- showAllChars is true. Clear them out chars to make sure no row shows for them.
		self.db.global.chars[currentRealm][currentChar] = nil
		self:TrimRaidTable()
	end
end

function Ailo:LevelCheck(info, newMinLevel)
	local playerLevel = UnitLevel("player")
	local oldMinLevel = self.db.profile.minLevel
	self.db.profile.minLevel = newMinLevel

	if playerLevel >= newMinLevel then
		if playerLevel < oldMinLevel then
			-- the setting change means this character is now elligible
			updateRetryTimer = self:ScheduleRepeatingTimer("DelayedInit", .2)
		end
	else -- playerLevel < newMinLevel
		if playerLevel >= oldMinLevel then
			-- the setting change means character should now be filtered
			self.db.global.chars[currentRealm][currentChar] = nil
			self:TrimRaidTable()
			self:OnDisable()
		end
	end
end

function Ailo:DelayedInit()
	-- When GetNextDailyReset is called early, like during OnEnable, it can 
	-- return hugeNumber. If a hugeNumber is saved as an expire time acedb
	-- will convert it to a nil. This is most often a problem for currency
	-- trackers which could only check once, during the first UpdatePlayer
	-- called during OnEnable. Solution is to use an Ace Timer callback to
	-- introduce a small delay between OnEnable and PlayerUpdate to give 
	-- the game time get the daily quest reset time sorted out.
	-- If we hit RetryLimit, stop delaying and just let it init.

	if updateRetryCount < updateRetryLimit then
		updateRetryCount = updateRetryCount + 1
		if GetNextDailyReset() == hugeNumber then
			return
		end
	end

	if updateRetryTimer then
		self:CancelTimer(updateRetryTimer, true)
		updateRetryCount = updateRetryLimit
		updateRetryTimer = nil
	end

	self:RegisterEvent("CHAT_MSG_SYSTEM",         "TriggerLockoutUpdate")  -- triggers UPDATE_INSTANCE_INFO event
	self:RegisterEvent("CURRENCY_DISPLAY_UPDATE", "UpdateCurrencies")
	self:RegisterEvent("LFG_COMPLETION_REWARD")   -- triggers LFG_UPDATE_RANDOM_INFO event
	self:RegisterEvent("LFG_UPDATE_RANDOM_INFO",  "UpdateLFGResets")
	self:RegisterEvent("SHOW_LOOT_TOAST",         "TriggerLockoutUpdate")  -- triggers UPDATE_INSTANCE_INFO event
	self:RegisterEvent("UNIT_QUEST_LOG_CHANGED")  -- calls UpdateQuests if unitID is player
	self:RegisterEvent("UPDATE_INSTANCE_INFO",    "UpdateLockouts")
	self:RegisterEvent("ZONE_CHANGED_NEW_AREA",   "CheckCurrentMap") 

	-- trigger collection of lockout, reset and quest information
	self:UpdatePlayer()
end   

function Ailo:OnDisable()
	self:CancelAllTimers()

	self:UnregisterEvent("CHAT_MSG_SYSTEM")
	self:UnregisterEvent("CURRENCY_DISPLAY_UPDATE")
	self:UnregisterEvent("LFG_COMPLETION_REWARD")
	self:UnregisterEvent("LFG_UPDATE_RANDOM_INFO")
	self:UnregisterEvent("SHOW_LOOT_TOAST")
	self:UnregisterEvent("UNIT_QUEST_LOG_CHANGED")
	self:UnregisterEvent("UPDATE_INSTANCE_INFO")
	self:UnregisterEvent("ZONE_CHANGED_NEW_AREA")
end

-- if we change the structure or contents of the saved settings, we can 
-- do a fixup of the data here the first time we run after the upgrade
function Ailo:MigrateSavedData()
	local userVersion = self.db.global.version

	-- if there is no version stamp, only safe thing to do is wipe it
	if not userVersion then
		Ailo:WipeDB()
		self.db.global.version = DB_VERSION 
	-- if version is 0 then its a new user and so there's nothing to update but the version number
	elseif userVersion == 0 then
		self.db.global.version = DB_VERSION
	-- if version is less current, settings may need to be migrated/tweaked/etc
	elseif userVersion < DB_VERSION then
		if userVersion < 9 then
			-- change to values stored for daily heroic 
			self:Output(L["DB_VERSION_UPGRADE_PURGE"])
			Ailo:WipeDB()

			-- if they weren't showing the old VP column, don't show the new
			self.db.profile.showWeeklyValor = self.db.profile.showDailyHeroic
		end

		if userVersion and userVersion < 10 then
			-- database does not need to be wiped for these changes

			-- if they were showing the daily PVP column, they'll likely want weekly CP tracking
			self.db.profile.showWeeklyConquest = self.db.profile.showDailyPVP
			-- remove dalaran weekly raid quest tracking
			self.db.profile.showWeeklyRaid = nil
		end

		if userVersion < 11 then
			-- change to values stored for naming convention normalization
			self:Output(L["DB_VERSION_UPGRADE_PURGE"])
			Ailo:WipeDB()

			-- name change to distinguish from daily heroic scenario
			if self.db.profile.showDailyHeroic ~= nil then
				self.db.profile.showDailyHeroicDungeon = self.db.profile.showDailyHeroic
				self.db.profile.showDailyHeroic = nil
			end

			-- show daily heroic scenario if already showing daily heroic dungeon or daily scenario
			self.db.profile.showDailyHeroicScenario = self.db.profile.showDailyHeroicDungeon or self.db.profile.showDailyScenario
		end

		if userVersion < 12 then
			-- only one toggle for world bosses now with blizz api change
			-- database does not need to be wiped for these changes
			self.db.profile.showShaOfAnger = nil
			self.db.profile.showGalleon = nil
			self.db.profile.showNalak = nil
			self.db.profile.showOondasta = nil
		end

		if userVersion < 13 then
			self:Output(L["DB_VERSION_UPGRADE_PURGE"])
			Ailo:WipeDB()
		end

		self.db.global.version = DB_VERSION 
	end
end

function Ailo:InitIconsTable()
	icons.WeeklyValor    = select(3, GetCurrencyInfo(Currencies.Valor.id))
	icons.WeeklyConquest = select(3, GetCurrencyInfo(Currencies.Conquest.id))
	icons.LootCoins      = select(3, GetCurrencyInfo(Currencies.LootCoins.id))

	-- icons.Justice  = select(3, GetCurrencyInfo(_G.JUSTICE_CURRENCY))
	-- icons.Honor    = select(3, GetCurrencyInfo(_G.HONOR_CURRENCY))

	icons.LFR = "Interface\\LFGFrame\\UI-LFR-PORTRAIT"
	icons.FlexRaid = "Interface\\Icons\\Achievement_General_StayClassy"
	-- icons.WorldBosses = "Interface\\minimap\\UI-Minimap-WorldMapSquare"
	icons.WorldBosses = "Interface\\Icons\\inv_misc_map02"

	icons.DailyHeroicScenario = "Interface\\Icons\\inv_misc_lockboxghostiron"
	icons.DailyHeroicDungeon  = "Interface\\Icons\\INV_Helmet_08"
	icons.DailyScenario       = "Interface\\Icons\\Icon_Scenarios"
	icons.DailyPVP            = "Interface\\PVPFrame\\Icon-Combat"

	icons.TBVictory  = select(3, GetCurrencyInfo(391))  -- Tol Barad Commendation
	icons.WGVictory  = select(3, GetCurrencyInfo(126))  -- Wintergrasp Mark of Honor

	-- very tempting to do the following, but then we won't be right if they are on
	-- as a seaonal window becomes active/inactive. need it to be checked dynamically
	-- instead of cached
	-- icons.Seasonal = Seasonal:Icon()
	
	icons.Tillers = "Interface\\Icons\\achievement_faction_tillers"
end

function Ailo:InitCurrenciesTable()
	for currency,info in pairs(Currencies) do
		-- GetCurrencyInfo returns weeklyMax = 100000 and totalMax = 300099 for VALOR_CURRENCY, 
		-- but the currentTotal and earnedThisWeek values are the expected magnitude.
		-- All values returned by CONQUEST_CURRENCY look fine.
		-- Hack a work around by adding a scaler value for currencies in the Currencies table.
		local _, _, _, _, weeklyMax, totalMax = GetCurrencyInfo(info.id)
		info.weeklyMax = floor(info.scaler * weeklyMax)
		info.totalMax  = floor(info.scaler *  totalMax)

		-- set the cached totals to -1 so first update will set up saved data
		info.weekly = -1
		info.total  = -1
	end
end

--=========================================================================--
--=========================================================================--
--
-- main data acquisition and processing function
--
--=========================================================================--
--=========================================================================--

function Ailo:UpdatePlayer()
	if self.db.profile.minLevel > UnitLevel("player") then return end
	self:PrepareCharacterTables()
	self.db.global.charClass[currentCharRealm] = select(2,UnitClass('player'))

	self:UpdateQuests()
	self:UpdateCurrencies()
	self:CheckCurrentMap()

	-- update lockouts for raids and instances by triggering an UPDATE_INSTANCE_INFO event
	RequestRaidInfo()

	-- update resets and LFG rewards by triggering an LFG_UPDATE_RANDOM_INFO event 
	RequestLFDPlayerLockInfo()

	-- ensure we don't miss a reset
	self.db.global.nextPurge = self:GetNextPurge()
end

--=========================================================================--
-- Currency Point Trackers
--=========================================================================--

-- event hander for CURRENCY_DISPLAY_UPDATE
function Ailo:UpdateCurrencies(event, ...)
	local char = self.db.global.chars[currentRealm][currentChar]
	local points = char.currencies
	local resets = char.resets
	local expire = GetNextWeeklyReset()
	local updateresets = false

	for currency, info in pairs(Currencies) do
		local _, total, _, weekly = GetCurrencyInfo(info.id)
		if total ~= info.total then
			-- may have uncapped by spending valor
			local wasCapped = info.total == info.totalMax or info.weekly == info.weeklyMax

			-- save and cache the new total amount
			points[currency] = total
			info.total = total

			-- Save the weekly amount as a weekly reset. The little dance with the tables
			-- is to ensure we don't blow away any data set by another tracking mechanism; 
			-- for example, the quests for gaining more LootCoins.
			local reset = resets[currency] or { expire = expire }
			reset.encounter = weekly
			resets[currency] = reset
			info.weekly = weekly

			local isCapped = info.total == info.totalMax or info.weekly == info.weeklyMax
			updateresets = updateresets or (wasCapped ~= isCapped)
		end
	end

	if updateresets then
		RequestLFDPlayerLockInfo()
	end
end

--=========================================================================--
-- Raid and Instance Lockouts
--=========================================================================--

local INSTANCE_SAVED = _G["INSTANCE_SAVED"]
function Ailo:TriggerLockoutUpdate(event, msg)
	if event == "CHAT_MSG_SYSTEM" and tostring(msg) == INSTANCE_SAVED then
		-- You are now saved to this instance; refresh Lockout info
		RequestRaidInfo()  -- triggers UPDATE_INSTANCE_INFO event
	elseif event == "SHOW_LOOT_TOAST" then
		if updateLockoutTimer then
			self:CancelTimer(updateLockoutTimer)
		end
		updateLockoutTimer = self:ScheduleTimer("TriggerLockoutUpdate", 10, "DelayedLockoutUpdate")
	elseif event == "DelayedLockoutUpdate" then
		-- self:Print("updating lockouts after delay")
		updateLockoutTimer = nil
		RequestRaidInfo()  -- triggers UPDATE_INSTANCE_INFO event
	end
end

-- event handler for UPDATE_INSTANCE_INFO, process all raid/instance lockouts
function Ailo:UpdateLockouts()
	-- if self.db.profile.minLevel > UnitLevel("player") then return end
	-- self.db.global.charClass[currentCharRealm] = select(2,UnitClass('player'))

	local now = time()

	for index=1, GetNumSavedInstances() do
		local instanceName, _, instanceReset, _, locked, _, _, isRaid, maxPlayers, _, maxEncounter, encounterDone = GetSavedInstanceInfo(index)
		if maxPlayers == 25 then 
			maxPlayers = 10 
		end
		if locked then
			local instanceString = strformat("%s.%d", instanceName, maxPlayers)
			self:ExtendRaidTable( instanceName, maxPlayers )
			self:SaveRaidForChar( instanceString, now+instanceReset, maxEncounter-encounterDone )
		end
	end

	local resets = self.db.global.chars[currentRealm][currentChar].resets
	local world  = self.db.global.world

	savedWorldBosses = saved
	for i=1, GetNumSavedWorldBosses() do
		local bossName, worldBossID, bossReset = GetSavedWorldBossInfo(i)
		local expire = now + bossReset
		resets[bossName] = { expire = expire }
		world[bossName] = max(expire, world[bossName] or 0)
	end
end

function Ailo:SaveRaidForChar(instance, expireTime, encounterLeft, character, realm)
	character     = character or currentChar
	realm         = realm or currentRealm
	encounterLeft = encounterLeft or 0

	local lockouts = self.db.global.chars[realm][character].lockouts
	lockouts[instance] = lockouts[instance] or {}
	lockouts[instance].expire    = expireTime
	lockouts[instance].encounter = encounterLeft

	if expireTime < self.db.global.nextPurge then
		self.db.global.nextPurge = expireTime
	end
end

--=========================================================================--
-- LFG Reward Resets
--=========================================================================--

function Ailo:LFG_COMPLETION_REWARD()
	--[[
	Fires when a random dungeon is completed and the achievement-like
	alert window pops up. The problem is that this DOES NOT update
	the return values of GetLFGDungeonRewards(x), those are updated
	when LFG_UPDATE_RANDOM_INFO is recieved, so force the client to
	call for an update which will be handled by UpdateLFGResets.
	]]--
	RequestLFDPlayerLockInfo()
end

-- event handler for LFG_UPDATE_RANDOM_INFO, process all weekly/daily instance resets
function Ailo:UpdateLFGResets(...)
	local resets = self.db.global.chars[currentRealm][currentChar].resets
	local lfrs   = self.db.global.lfrs
	local world  = self.db.global.world

	-- 462 random mists of pandaria heroic dungeon
	-- 463 random mists of pandaria dungeon
	-- 493 random mists of pandaria scenario
	-- 641 random mists of pandaria heroic scenario

	-- cell is red only if instance type is heroic and if done today or has no additional rewards (means money only, no valor)
	local bestchoice = GetRandomDungeonBestChoice() or 462
	if select(LFG_RETURN_VALUES.difficulty, GetLFGDungeonInfo(bestchoice)) > 0 then
		local doneToday, _, _, _, _, numRewards = GetLFGDungeonRewards(462)
		if numRewards < 1 then  -- if not returning any valor then we must be capped for the week
			resets.DailyHeroicDungeon = { expire = GetNextWeeklyReset() }
		elseif doneToday then
			resets.DailyHeroicDungeon = { expire = GetNextDailyReset() }
		else -- if they hit total cap we mark as done, need to clear that if they spend points and drop under caps again
			resets.DailyHeroicDungeon = nil
		end
	end

	-- daily heroic scenario first completion bonus
	local doneToday, _, _, _, _, numRewards = GetLFGDungeonRewards(641)
	if numRewards < 2 then  -- if not returning any valor then we must be capped for the week
		resets.DailyHeroicScenario = { expire = GetNextWeeklyReset() }
	elseif doneToday then
		resets.DailyHeroicScenario = { expire = GetNextDailyReset() }
	else -- if they hit total cap we mark as done, need to clear that if they spend points and drop under caps again
		resets.DailyHeroicScenario = nil
	end

	-- daily scenario first completion bonus
	local doneToday, _, _, _, _, numRewards = GetLFGDungeonRewards(493)
	if numRewards < 2 then  -- if not returning any valor then we must be capped for the week
		resets.DailyScenario = { expire = GetNextWeeklyReset() }
	elseif doneToday then
		resets.DailyScenario = { expire = GetNextDailyReset() }
	else -- if they hit total cap we mark as done, need to clear that if they spend points and drop under caps again
		resets.DailyScenario = nil
	end

	-- daily battleground first win bonus
	if GetRandomBGHonorCurrencyBonuses() then
		resets.DailyPVP = { expire = GetNextDailyReset() }
	end

	local id = Seasonal:Dungeon()
	if (id and GetLFGDungeonRewards(id)) then
		resets.Seasonal = { expire = GetNextDailyReset() }
	end

	for _, rf in pairs(RaidFinder) do
		local _, killed = GetLFGDungeonNumEncounters(rf.id)
		if killed > 0 then
			local expire = rf.GetExpireTime()
			resets[rf.id] = { expire = expire, encounter = (rf.count - killed), done = GetLFGDungeonRewards(rf.id) }
			lfrs[rf.id] = max(expire, lfrs[rf.id] or 0)
		end
	end
end

--=========================================================================--
-- Daily/Weekly Repeatable Quests
--=========================================================================--

function Ailo:UNIT_QUEST_LOG_CHANGED(event, unitID, ...)
	if unitID == "player" then
		self:UpdateQuests()
	end
end

function Ailo:UpdateQuests()
	local resets = self.db.global.chars[currentRealm][currentChar].resets

	for objective,data in pairs(RepeatableQuests) do
		for _,id in pairs (data.questIds) do
			if IsQuestFlaggedCompleted(id) then
				local reset = resets[objective] or { expire = data.GetExpireTime() }
				reset.done = true
				resets[objective] = reset
				break
			end
		end
	end
end


function Ailo:CheckCurrentMap(...)
	local curMapID = GetCurrentMapAreaID() 
	if curMapID == 807 then -- 807 == valley of the four winds
		self:RegisterEvent("UNIT_SPELLCAST_SUCCEEDED")
	else
		self:UnregisterEvent("UNIT_SPELLCAST_SUCCEEDED")
	end
end

function Ailo:UNIT_SPELLCAST_SUCCEEDED(event, unitID, spell, rank, lineID, spellID, ...)
	if unitID == "player" then
		-- self:Print(event, unitID, spell, rank, lineID, spellID, ...)
		if
			130140 == spellID or -- harvest autumn blossom tree
			129674 == spellID or -- harvest fool's cap
			129673 == spellID or -- harvest golden lotus
			111123 == spellID or -- harvest green cabbage
			129687 == spellID or -- harvest green tea leaf
			130025 == spellID or -- harvest jade squash
			123353 == spellID or -- harvest juicycrunch carrot
			129796 == spellID or -- harvest magebulb
			123445 == spellID or -- harvest mogu pumpkin
			123548 == spellID or -- harvest pink turnip
			123355 == spellID or -- harvest plump green cabbage
			130026 == spellID or -- harvest plump jade squash
			123356 == spellID or -- harvest plump juicycruch carrot
			123451 == spellID or -- harvest plump mogu pumpkin
			123549 == spellID or -- harvest plump pink turnip
			123522 == spellID or -- harvest plump red blossom leak
			123380 == spellID or -- harvest plump scallions
			130043 == spellID or -- harvest plump striped melon
			123571 == spellID or -- harvest plump white turnip
			129984 == spellID or -- harvest plump witchberries
			133106 == spellID or -- harvest portal shard
			129705 == spellID or -- harvest rain poppy
			129843 == spellID or -- harvest raptorleaf
			123524 == spellID or -- harvest red blossom leak
			123375 == spellID or -- harvest scallions
			129676 == spellID or -- harvest silkweed
			129757 == spellID or -- harvest snakeroot
			129675 == spellID or -- harvest snow lily
			129887 == spellID or -- harvest songbell
			130168 == spellID or -- harvest spring blossom tree
			130042 == spellID or -- harvest striped melon
			130109 == spellID or -- harvest terrible turnip
			123570 == spellID or -- harvest white turnip
			129814 == spellID or -- harvest windshear cactus
			123516 == spellID or -- harvest winter blossom tree
			129983 == spellID    -- harvest witchberries
		then
			local resets = self.db.global.chars[currentRealm][currentChar].resets
			local crops = resets.Tillers or { expire = GetNextDailyReset(), encounter = 0, done = false }

			crops.encounter = crops.encounter + 1
			if crops.encounter == 16 then 
				crops.done = true 
			end
			resets.Tillers = crops
		end
	end
end

--=========================================================================--
--=========================================================================--
--
-- saved data maintenance functions
--
--=========================================================================--
--=========================================================================--

function Ailo:PurgeExpiredData()
	local now = time()

	for realm, chars in pairs(self.db.global.chars) do 
		for char, charData in pairs(chars) do 
			if charData.lockouts then 
				for instance, instanceData in pairs(charData.lockouts) do
					if type(instanceData) ~= "table" or not instanceData.expire or now > instanceData.expire then
						self.db.global.chars[realm][char].lockouts[instance] = nil
					end
				end
			end
			if charData.resets then 
				for instance, instanceData in pairs(charData.resets) do
					if type(instanceData) ~= "table" or not instanceData.expire or now > instanceData.expire then
						self.db.global.chars[realm][char].resets[instance] = nil
					end
				end
			end
		end
	end

	local lfrsdb  = self.db.global.lfrs
	for id, expire in pairs(lfrsdb) do
		if now > expire then
			lfrsdb[id] = nil
		end
	end

	local worlddb  = self.db.global.world
	for id, expire in pairs(worlddb) do
		if now > expire then
			worlddb[id] = nil
		end
	end
end

function Ailo:GetNextPurge()
	local nextPurge = GetNextDailyReset()

	for realm, chars in pairs(self.db.global.chars) do 
		for _, charData in pairs(chars) do 
			if charData.lockouts then
				for _, instanceData in pairs(charData.lockouts) do
					nextPurge = min(nextPurge, instanceData.expire)
				end
			end
			if charData.resets then
				for _, instanceData in pairs(charData.resets) do
					nextPurge = min(nextPurge, instanceData.expire)
				end
			end
		end
	end

	for _, expire in pairs(self.db.global.lfrs) do
		nextPurge = min(nextPurge, expire)
	end

	for _, expire in pairs(self.db.global.world) do
		nextPurge = min(nextPurge, expire)
	end

	return nextPurge
end

function Ailo:WipeDB()
	if type(self.db.global.chars) == "table" then 
		wipe(self.db.global.chars)
	end
	if type(self.db.global.raids) == "table" then 
		wipe(self.db.global.raids)
	end
	if type(self.db.global.lfrs) == "table" then 
		wipe(self.db.global.lfrs)
	end
	if type(self.db.global.world) == "table" then 
		wipe(self.db.global.world)
	end
	if type(self.db.global.charClass) == "table" then 
		wipe(self.db.global.charClass)
	end
	self.db.global.nextPurge = GetNextDailyReset()
end

function Ailo:PrepareCharacterTables()
	if not self.db.global.chars[currentRealm] then
		self.db.global.chars[currentRealm] = {}
	end
	if not self.db.global.chars[currentRealm][currentChar] then
		self.db.global.chars[currentRealm][currentChar] = {}
	end
	if not self.db.global.chars[currentRealm][currentChar].lockouts then
		self.db.global.chars[currentRealm][currentChar].lockouts = {}
	end
	if not self.db.global.chars[currentRealm][currentChar].resets then
		self.db.global.chars[currentRealm][currentChar].resets = {}
	end
	if not self.db.global.chars[currentRealm][currentChar].currencies then
		self.db.global.chars[currentRealm][currentChar].currencies = {}
	end
end


function Ailo:ExtendRaidTable(instanceName, size)
	-- Save it for the tooltip
	self.db.global.raids[instanceName] = self.db.global.raids[instanceName] or {}
	self.db.global.raids[instanceName][size] =  true
end

function Ailo:TrimRaidTable()
	local raidsdb = self.db.global.raids
	for raid, sizes in pairs(raidsdb) do
		for size in pairs(sizes) do
			local isUsed = false
			local raidString = strformat("%s.%d", raid, size)
			for realm, chars in pairs(self.db.global.chars) do 
				for char, charData in pairs(chars) do
					if charData.lockouts and charData.lockouts[raidString] and charData.lockouts[raidString].expire then
						isUsed = true
						break
					end
				end
			end
			if not isUsed then
				raidsdb[raid] = nil
			end
		end
	end
end


function Ailo:ManualPlayerUpdate()
	if self.db.profile.minLevel > UnitLevel("player") then return end
	self:Output(L["Updating data for current player."])

	if not self.db.global.chars[currentRealm] then 
		self.db.global.chars[currentRealm] = {}
	else
		wipe(self.db.global.chars[currentRealm][currentChar])
		self.db.global.chars[currentRealm][currentChar] = nil
	end

	self:TrimRaidTable()
	self:UpdatePlayer()
end


function Ailo:GetInstanceAbbr(instanceName)
	if not self.db.profile.instanceAbbr[instanceName] then
		-- Has no abbreviation yet, try it with a somewhat good guess
		-- Tries to get the first char of every word, does not go well with utf-8 chars
		self.db.profile.instanceAbbr[instanceName] = strgsub(instanceName, "(%a)[%l%p]*[%s%-]*", "%1")
	end

	return ( self.db.profile.instanceAbbr[instanceName] ~= "" and self.db.profile.instanceAbbr[instanceName] or nil )
end


function Ailo:SetupClasscoloredFonts()
	local class, color, CHOOSEN_CLASS_COLORS

	if self.db.profile.useCustomClassColors and CUSTOM_CLASS_COLORS then
		CHOOSEN_CLASS_COLORS = CUSTOM_CLASS_COLORS
	else
		CHOOSEN_CLASS_COLORS = RAID_CLASS_COLORS
	end
	for class,color in pairs(CHOOSEN_CLASS_COLORS) do
		if not RAID_CLASS_COLORS_FONTS[class] then 
			RAID_CLASS_COLORS_FONTS[class] = CreateFont("ClassFont"..class)
			RAID_CLASS_COLORS_FONTS[class]:CopyFontObject(GameTooltipText)
		end
		RAID_CLASS_COLORS_FONTS[class]:SetTextColor(color.r, color.g, color.b)
	end
end


function Ailo:BuildSortedKeyTables()
	local c, r
	wipe(sortRealms)
	sortRealms = {}
	wipe(sortRealmsPlayer)
	sortRealmsPlayer = {}
	for r,_ in pairs(self.db.global.chars) do
		tinsert(sortRealms, r)
		sortRealmsPlayer[r] = {}
		for c,_ in pairs(self.db.global.chars[r]) do
			tinsert(sortRealmsPlayer[r],c)
		end
		tsort(sortRealmsPlayer[r])
	end
	tsort(sortRealms)
end


-- Seasonal Quest Helper functions

function Seasonal:GetCurrentEvent()
	local now = time()

	for k, event in pairs(self.Events) do
		if ( event.first <= now and now <= event.last) then
			return event
		end
	end

	return nil
end

function Seasonal:LevelAdjustment()
	local event = self:GetCurrentEvent()
	return (event ~= nil) and event.level or 0
end

function Seasonal:Show()
	return Ailo.db.profile.showSeasonal and self:GetCurrentEvent() ~= nil
end

function Seasonal:Icon()
	local event = self:GetCurrentEvent()
	return (event ~= nil) and event.icon or nil
end

function Seasonal:Dungeon()
	local event = self:GetCurrentEvent()
	return (event ~= nil) and event.dungeon_id or nil
end

function Seasonal:Quests()
	local event = self:GetCurrentEvent()
	return (event ~= nil) and event.quest_ids or nil
end

--=========================================================================--
--=========================================================================--
--
-- Tooltip and Options Display
--
--=========================================================================--
--=========================================================================--

local CellFormat = {}

CellFormat.LootCoins = function(currency, tooltip, line, column, char, color)
		local total = char.currencies[currency]
		if total then
			tooltip:SetCell(line, column, total)
		end

		if char.resets[currency] and char.resets[currency].done then
			tooltip:SetCellColor(line, column, color.r, color.g, color.blue, color.a)
		end
		
	end
	
CellFormat.DualCappedCurrency = function(currency, tooltip, line, column, char, color)
		local info = Currencies[currency]

		local total = char.currencies[currency]
		local totalLeft = info.totalMax - total
		
		local weekly = char.resets[currency] and char.resets[currency].encounter or 0
		local weeklyLeft = info.weeklyMax - weekly
		
		if totalLeft == 0 then
			-- hit total cap, color cell red and show total amount so user can see this is different from weekly capping
			tooltip:SetCell(line, column, total)
			tooltip:SetCellColor(line, column, color.r, color.g, color.blue, color.a)
		elseif weeklyLeft == 0 then
			-- hit weekly cap, color cell red but show no number; user is in a good place
			tooltip:SetCell(line, column, "")
			tooltip:SetCellColor(line, column, color.r, color.g, color.blue, color.a)
		elseif weeklyLeft >= totalLeft then
			-- user will hit total cap before weekly cap, color cell green and show total amount
			tooltip:SetCell(line, column, total)
		else 
			-- will hit weekly cap before total, color cell green and show amount earned this week
			weekly = weekly > 0 and weekly or ""
			tooltip:SetCell(line, column, weekly)
		end
	end


--=========================================================================--
-- PrepareTooltip - build and formate the tooltip to show lockout state
--=========================================================================--

function Ailo:PrepareTooltip(tooltip)
	local raidorder = {}
	--[[[ Cell are just colored green/red
		  Foo BaR Etc EtC ETC
	Char1 [ ] [ ] [ ] [ ] [ ]
	Char2 [ ] [ ] [ ] [ ] [ ]
	Char3 [ ] [ ] [ ] [ ] [ ]
	]]--
	local charsdb = self.db.global.chars
	local raidsdb = self.db.global.raids
	local lfrsdb  = self.db.global.lfrs
	local worlddb = self.db.global.world
	local profile = self.db.profile

	if time() > self.db.global.nextPurge then
		-- Only search 
		self:PurgeExpiredData()
		self:TrimRaidTable()
		self.db.global.nextPurge = self:GetNextPurge()
	end

	if profile.showAllChars or next(raidsdb) or 
	   (profile.showFlexRaid    and next(lfrsdb)) or 
	   (profile.showRaidFinder  and next(lfrsdb)) or 
	   (profile.showWorldBosses and next(worlddb)) or 
	   Seasonal:Show() or profile.showWeeklyValor or 
	   profile.showDailyHeroicDungeon or profile.showDailyScenario or 
	   profile.showDailyHeroicScenario or 
	   profile.showWGVictory or profile.showTBVictory or 
	   profile.showWeeklyConquest or profile.showDailyPVP then
		-- At least one char is saved to some 
		tooltip:AddHeader(L["Raid"]) -- Raid

		local columns = {}
		local raidfinder_cols = {}

		-- Seasonal column
		if Seasonal:Show() then
			columns.Seasonal = tooltip:AddColumn("CENTER")
			tooltip:SetCell(1, columns.Seasonal, strformat("|T%s:0|t", Seasonal:Icon()))
		end

		-- Loot Coins column
		if profile.showLootCoins then
			columns.LootCoins = tooltip:AddColumn("CENTER")
			tooltip:SetCell(1, columns.LootCoins, strformat("|T%s:0|t", icons.LootCoins))
		end

		-- Weekly Valor column
		if profile.showWeeklyValor then
			columns.WeeklyValor = tooltip:AddColumn("CENTER")
			tooltip:SetCell(1, columns.WeeklyValor, strformat("|T%s:0|t", icons.WeeklyValor))
		end

		-- Daily Heroic Scenario column
		if profile.showDailyHeroicScenario then
			columns.DailyHeroicScenario = tooltip:AddColumn("CENTER")
			tooltip:SetCell(1, columns.DailyHeroicScenario, strformat("|T%s:0|t", icons.DailyHeroicScenario))
		end

		-- Daily Heroic Dungeon column
		if profile.showDailyHeroicDungeon then
			columns.DailyHeroicDungeon= tooltip:AddColumn("CENTER")
			tooltip:SetCell(1, columns.DailyHeroicDungeon, strformat("|T%s:0|t", icons.DailyHeroicDungeon))
		end

		-- Daily Scenario column
		if profile.showDailyScenario then
			columns.DailyScenario = tooltip:AddColumn("CENTER")
			tooltip:SetCell(1, columns.DailyScenario, strformat("|T%s:0|t", icons.DailyScenario))
		end

		-- Weekly Conquest column
		if profile.showWeeklyConquest then
			columns.WeeklyConquest = tooltip:AddColumn("CENTER")
			tooltip:SetCell(1, columns.WeeklyConquest, strformat("|T%s:0|t", icons.WeeklyConquest))
		end

		-- PvP Daily column
		if profile.showDailyPVP then
			columns.DailyPVP = tooltip:AddColumn("CENTER")
			tooltip:SetCell(1, columns.DailyPVP, strformat("|T%s:0|t", icons.DailyPVP))
		end

		-- Tillers column
		if profile.showTillers then
			columns.Tillers = tooltip:AddColumn("CENTER")
			tooltip:SetCell(1, columns.Tillers, strformat("|T%s:0|t", icons.Tillers))
		end

		-- Wintergrasp Victory column
		if profile.showWGVictory then
			columns.WGVictory = tooltip:AddColumn("CENTER")
			tooltip:SetCell(1, columns.WGVictory, strformat("|T%s:0|t", icons.WGVictory))
		end
		-- Tol Barad Victory column
		if profile.showTBVictory then
			columns.TBVictory = tooltip:AddColumn("CENTER")
			tooltip:SetCell(1, columns.TBVictory, strformat("|T%s:0|t", icons.TBVictory))
		end

		-- World Bosses columns
		if profile.showWorldBosses then
			for boss,_ in pairs(worlddb) do
				local abbr = strformat("|T%s:0|t%s", icons.WorldBosses, self:GetInstanceAbbr(boss))
				local column = tooltip:AddColumn("CENTER")
				columns[boss] = column
				tooltip:SetCell(1, column, abbr)
			end
		end

		-- Flexible Raid columns
		if profile.showFlexRaid then
			for _,rf in ipairs(RaidFinder) do
				if lfrsdb[rf.id] and rf.flex then
					local name = GetLFGDungeonInfo(rf.id)
					local abbr = strformat("|T%s:0|t%s", icons.FlexRaid, self:GetInstanceAbbr(name))
					local column = tooltip:AddColumn("CENTER")
					raidfinder_cols[rf] = column
					tooltip:SetCell(1, column, abbr)
				end
			end
		end

		-- Raid Finder columns
		if profile.showRaidFinder then
			for _,rf in ipairs(RaidFinder) do
				if lfrsdb[rf.id] and not rf.flex then
					local name = GetLFGDungeonInfo(rf.id)
					local abbr = strformat("|T%s:0|t%s", icons.LFR, self:GetInstanceAbbr(name))
					local column = tooltip:AddColumn("CENTER")
					raidfinder_cols[rf] = column
					tooltip:SetCell(1, column, abbr)
				end
			end
		end

		-- Instances with lockouts
		for raid, sizes in pairs(raidsdb) do
			local raidabbr = self:GetInstanceAbbr(raid)
			for size, _ in pairs(sizes) do
				if raidabbr and ( size > 5 or self.db.profile.show5Man ) then
					local lastcolumn = tooltip:AddColumn("CENTER")
					raidorder[( strformat("%s.%d", raid, size))] = lastcolumn
					tooltip:SetCell(1, lastcolumn , raidabbr)
				end
			end
		end
		self:BuildSortedKeyTables()
		local lastline
		for _, realm in ipairs(sortRealms) do
			if profile.showRealmHeaderLines then
				lastline = tooltip:AddLine("")
				tooltip:SetCell(lastline, 1, realm, nil, "CENTER", tooltip:GetColumnCount())
			end
			for _, char in ipairs(sortRealmsPlayer[realm]) do
				local charData = charsdb[realm][char]
				if profile.showAllChars or next(charData.lockouts) or next(charData.resets) then

					local charRealm = char.." - "..realm
					local lineName = profile.showCharacterRealm and charRealm or char
					local lineFont = profile.useClassColors and RAID_CLASS_COLORS_FONTS[ self.db.global.charClass[charRealm] ] or nil
					lastline = tooltip:AddLine("")
					tooltip:SetCell(lastline, 1, lineName, lineFont)

					-- init cells
					for i = tooltip:GetColumnCount(),2,-1 do
						tooltip:SetCell(lastline, i, "")
						tooltip:SetCellColor(lastline, i, profile.freeraid.r, profile.freeraid.g, profile.freeraid.b, profile.freeraid.a)
					end

					if columns.Seasonal and charData.resets.Seasonal then
						tooltip:SetCellColor(lastline, columns.Seasonal, profile.savedraid.r, profile.savedraid.g, profile.savedraid.b, profile.savedraid.a)
					end

					if columns.LootCoins then 
						CellFormat.LootCoins("LootCoins", tooltip, lastline, columns.LootCoins, charData, profile.savedraid)
					end

					if columns.WeeklyValor then 
						CellFormat.DualCappedCurrency("Valor", tooltip, lastline, columns.WeeklyValor, charData, profile.savedraid)
					end

					if columns.DailyHeroicScenario and charData.resets.DailyHeroicScenario then 
						tooltip:SetCellColor(lastline, columns.DailyHeroicScenario, profile.savedraid.r, profile.savedraid.g, profile.savedraid.b, profile.savedraid.a)
					end

					if columns.DailyHeroicDungeon and charData.resets.DailyHeroicDungeon then 
						tooltip:SetCellColor(lastline, columns.DailyHeroicDungeon, profile.savedraid.r, profile.savedraid.g, profile.savedraid.b, profile.savedraid.a)
					end

					if columns.DailyScenario and charData.resets.DailyScenario then 
						tooltip:SetCellColor(lastline, columns.DailyScenario, profile.savedraid.r, profile.savedraid.g, profile.savedraid.b, profile.savedraid.a)
					end

					if columns.Tillers and charData.resets.Tillers then 
						if charData.resets.Tillers.done then
							tooltip:SetCellColor(lastline, columns.Tillers, profile.savedraid.r, profile.savedraid.g, profile.savedraid.b, profile.savedraid.a)
						elseif charData.resets.Tillers.encounter > 0 then
							tooltip:SetCell(lastline, columns.Tillers, charData.resets.Tillers.encounter)
						end
					end

					for boss,_ in pairs(worlddb) do
						if columns[boss] and charData.resets[boss] then
							tooltip:SetCellColor(lastline, columns[boss], profile.savedraid.r, profile.savedraid.g, profile.savedraid.b, profile.savedraid.a)
						end
					end

					for rf, column in pairs(raidfinder_cols) do
						local id = rf.id
						if charData.resets[id] then
							if charData.resets[id].encounter == 0 then
								tooltip:SetCellColor(lastline, column, profile.savedraid.r, profile.savedraid.g, profile.savedraid.b, profile.savedraid.a)
							elseif profile.showLeft then
								local strCell = strformat("%s%i", charData.resets[id].done and "" or strformat("|T%s:0|t ", icons.WeeklyValor), charData.resets[id].encounter)
								tooltip:SetCell(lastline, column, strCell)
							end
						end
					end

					if columns.WeeklyConquest then 
						CellFormat.DualCappedCurrency("Conquest", tooltip, lastline, columns.WeeklyConquest, charData, profile.savedraid)
					end

					if columns.DailyPVP and charData.resets.DailyPVP then
						tooltip:SetCellColor(lastline, columns.DailyPVP, profile.savedraid.r, profile.savedraid.g, profile.savedraid.b, profile.savedraid.a)
					end
					if columns.WGVictory and charData.resets.WGVictory then
						tooltip:SetCellColor(lastline, columns.WGVictory, profile.savedraid.r, profile.savedraid.g, profile.savedraid.b, profile.savedraid.a)
					end
					if columns.TBVictory and charData.resets.TBVictory then
						tooltip:SetCellColor(lastline, columns.TBVictory, profile.savedraid.r, profile.savedraid.g, profile.savedraid.b, profile.savedraid.a)
					end
					for instance, instanceData in pairs(charData.lockouts) do
						if raidorder[instance] then
							tooltip:SetCellColor(lastline, raidorder[instance], profile.savedraid.r, profile.savedraid.g, profile.savedraid.b, profile.savedraid.a)
							if instanceData.encounter > 0 and profile.showLeft then
								tooltip:SetCell(lastline, raidorder[instance], instanceData.encounter)
							end
						end
					end
				end
			end
		end
	else
		-- No saved raids at all
		tooltip:AddHeader(L["No saved raids found"])
	end
end


--=========================================================================--
-- GenerateOptions - build options UI table
--=========================================================================--

function Ailo.GenerateOptions()
	if not Ailo.options then
		local function newCounter()
			local i = 0
			return function()
					i = i + 1
					return i
				end
		end
		local count = newCounter()

		Ailo.options = {
			name = "Ailo",
			type = 'group',
			handler = Ailo,
			args = {
				genconfig = {
					name = L["General Settings"],
					type = 'group',
					order = 1,
					get = function(info) return Ailo.db.profile[info[#info]] end,
					set = function(info, value) Ailo.db.profile[info[#info]] = value end,
					args = {
						savedraid = {
							name = L["Saved raid color"],
							desc = L["SAVED_RAID_DESC"],
							type = 'color',
							order = count(),
							get  = getColor,
							set  = setColor,
							hasAlpha = true,
						},
						freeraid = {
							name = L["Free raid color"],
							desc = L["FREE_RAID_DESC"],
							type = 'color',
							order = count(),
							get  = getColor,
							set  = setColor,
							hasAlpha = true,
						},
						useClassColors = {
							type = "toggle",
							order = count(),
							name = L["Color names by class"],
						},
						useCustomClassColors = {
							type = "toggle",
							order = count(),
							name = L["Use !ClassColors"],
							desc = L["Use !ClassColors addon for class colors used to color the names in the tooltip"],
							get = function(info) return Ailo.db.profile[info[#info]] end,
							set = function(info, value) 
								Ailo.db.profile[info[#info]] = value 
								Ailo:SetupClasscoloredFonts()
							end,
							disabled = function() return not Ailo.db.profile.useClassColors or not CUSTOM_CLASS_COLORS end,
						},
						showRealmHeaderLines  = {
							type = "toggle",
							order = count(),
							name = L["Show Realm Headers"],
							desc = L["SHOW_REALMLINES_DESC"],
						},
						showCharacterRealm = {
							name = L["Show character realms"],
							type = "toggle",
							order = count(),
						},
						minimapIcon = {
							type = "toggle",
							name = L["Show minimap button"],
							desc = L["Show the Ailo minimap button"],
							order = count(),
							get = function(info) return not Ailo.db.profile.minimapIcon.hide end,
							set = function(info, value)
								Ailo.db.profile.minimapIcon.hide = not value
								if value then LDBIcon:Show("Ailo") else LDBIcon:Hide("Ailo") end
							end,
						},
						showMessages = {
							type = "toggle",
							order = count(),
							name = L["Chatframe Messages"],
						},
						hc = {
							type = "input",
							order = count(),
							disabled = true,
							hidden = true,
							name = L["Tooltip abbreviation used for heroic raids"],
							width = "double",
						},
						nhc = {
							type = "input",
							order = count(),
							disabled = true,
							hidden = true,
							name = L["Tooltip abbreviation used for nonheroic raids"],
							width = "double",
						},
						weeklyResetDay = {
							type = "select",
							order = count(),
							name = L["Weekly reset day"],
							desc = L["Day when weekly caps and lockouts are reset"],
							values = function() return { CalendarGetWeekdayNames() } end,
							get = function(info) return Ailo.db.global.weeklyResetDay end,
							set = function(info, value) Ailo.db.global.weeklyResetDay = value end,
						},
						wipeDB = {
							type = "execute",
							name = L["Wipe Database"],
							order = count(),
							confirm = true,
							func = function() 
									Ailo:WipeDB() 
									Ailo:UpdatePlayer()
								end,
						},
						showAllChars  = {
							type = "toggle",
							order = count(),
							name = L["Show all chars"],
							desc = L["Regardles of any saved instances"],
						},                    
						showLeft = {
							type = "toggle",
							order = count(),
							name = L["Encounters Left"],
							desc = L["SHOW_ENCOUTERS_LEFT_DESC"],
						},
						minLevel = {
							type = "range",
							order = count(),
							min = 1;
							max = MAX_PLAYER_LEVEL_TABLE[GetExpansionLevel()];
							step = 1,
							name = L["Minimum Level"],
							desc = L["Characters below this level will not be shown"],
							set = "LevelCheck",
						},


						headerTrackers = {
							type = "header",
							order = count(),
							name = "",
						},


						showSeasonal  = {
							type = "toggle",
							order = count(),
							image = Seasonal:Icon(),
							name = L["Track Seasonal bosses"],
							desc = L["If the character has received the reward for a seasonal boss"],
						},
						showLootCoins = {
							type = "toggle",
							order = count(),
							image = icons.LootCoins,
							name = L["Track Warforged Seals"],
							desc = L["Show number of Warforged Seals and whether weekly quest has been completed"],
						},
						showWeeklyValor = {
							type = "toggle",
							order = count(),
							image = icons.WeeklyValor,
							name = L["Track Valor Points"],
							desc = L["TRACK_VALOR_POINTS_DESC"],
						},
						showDailyHeroicScenario = {
							type = "toggle",
							order = count(),
							image = icons.DailyHeroicScenario,
							name = L["Track 'Daily Heroic Scenario'"],
							desc = L["TRACK_DAILY_HEROIC_SCENARIO_DESC"],
						},
						showDailyHeroicDungeon = {
							type = "toggle",
							order = count(),
							image = icons.DailyHeroicDungeon,
							name = L["Track 'Daily Heroic'"],
							desc = L["TRACK_DAILY_HEROIC_DESC"],
						},
						showDailyScenario = {
							type = "toggle",
							order = count(),
							image = icons.DailyScenario,
							name = L["Track 'Daily Scenario'"],
							desc = L["TRACK_DAILY_SCENARIO_DESC"],
						},
						showTillers = {
							type = "toggle",
							order = count(),
							image = icons.Tillers,
							name = L["Track crops"],
							desc = L["If the character has gathered crops off their farm today"],
						},
						showWorldBosses = {
							type = "toggle",
							order = count(),
							image = icons.WorldBosses,
							name = L["Track World Bosses"],
							desc = L["Tracks any World Bosses from which loot has been collected this week"],
						},
						showFlexRaid = {
							type = "toggle",
							order = count(),
							image = icons.FlexRaid,
							name = L["Track Flex Raid instances"],
							desc = L["Tracks any raid done as a Flexible Raid group"],
						},
						showRaidFinder = {
							type = "toggle",
							order = count(),
							image = icons.LFR,
							name = L["Track Raid Finder instances"],
							desc = L["Tracks any raid done through the Raid Finder tool"],
						},
						show5Man = {
							type = "toggle",
							order = count(),
							name = L["Show 5-man instances"],
						},
						Padding_PVEPVPSeparator = {
							order = count(),
							type = "description",
							name = "",
						},

						showWeeklyConquest = {
							type = "toggle",
							order = count(),
							image = icons.WeeklyConquest,
							name = L["Track Conquest Points"],
							desc = L["TRACK_CONQUEST_POINTS_DESC"],
						},
						showDailyPVP  = {
							type = "toggle",
							order = count(),
							image = icons.DailyPVP,
							name = L["Track PvP daily"],
							desc = L["TRACK_DAILY_PVP_DESC"],
						},

						showWGVictory  = {
							type = "toggle",
							order = count(),
							image = icons.WGVictory,
							name = L["Track 'WG Victory'"],
							desc = L["If the character has done the 'Victory in Wintergrasp' weekly pvp quest"],
						},
						showTBVictory  = {
							type = "toggle",
							order = count(),
							image = icons.TBVictory,
							name = L["Track 'TB Victory'"],
							desc = L["If the character has done the 'Victory in Tol Barad' weekly pvp quest"],
						},
					},
				},
				instanceAbbr = { 
					type = 'group',
					name = L["Instance Abbreviations"],
					get = function(info) return Ailo.db.profile.instanceAbbr[info[#info]] end,
					set = function(info, value) Ailo.db.profile.instanceAbbr[info[#info]] = value end,
					args = {
						header = {
							type = "header",
							order = 1,
							name = L["Change the abbreviations used in the tooltip"]
						},
					},
				},
			},
		}
		local instance, abbr
		for instance, abbr in pairs(Ailo.db.profile.instanceAbbr) do
			Ailo.options.args.instanceAbbr.args[instance] = {
				type = "input",
				name = instance,
			}
		end
		Ailo.options.args.profile = LibStub("AceDBOptions-3.0"):GetOptionsTable(Ailo.db)
	end

	return Ailo.options
end
