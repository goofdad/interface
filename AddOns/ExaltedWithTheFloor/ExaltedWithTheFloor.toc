## Author: Crazysheep - Vek'nilash EU
## Interface: 50400
## Title: Exalted with the Floor
## Notes: Time to see how good you are and try not to get exalted with this rep!
## Version: 1.0
## SavedVariables: ExaltedWithFloorDB
## OptionalDeps: LibStub, CallbackHandler-1.0

#@no-lib-strip@
Libs\LibStub\LibStub.lua
Libs\CallbackHandler-1.0\CallbackHandler-1.0.lua
#@end-no-lib-strip@

embeds.xml

layout.xml
update.lua