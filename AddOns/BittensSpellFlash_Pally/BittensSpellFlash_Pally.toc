## Interface: 50400
## Version: 50400.2.1
## Author: Xemnosyst
## Title: Bitten's SpellFlash: Pally
## Notes: Replaces Blizzard's default proc highlighting to flash 1) a maximum dps rotation, 2) a maximum survivability tanking rotation, or 3) more informative healing proc highlighting.
## URL: http://www.curse.com/addons/wow/bittens-spellflash-pally
## Dependencies: SpellFlash
## OptionalDeps: BittensSpellFlashLibrary
## LoadOnDemand: 1
## X-SpellFlashAddon-LoadWith: Paladin
## X-Curse-Packaged-Version: 50400.2.1
## X-Curse-Project-Name: Bitten's SpellFlash: Pally
## X-Curse-Project-ID: bittens-spellflash-pally
## X-Curse-Repository-ID: wow/bittens-spellflash-pally/mainline
utils\BittensUtils.xml
bsl\BittensSpellFlashLibrary.xml
src\Localization.lua
src\IDs.lua
src\Spells.lua
src\Rotations.lua
src\Options.lua
