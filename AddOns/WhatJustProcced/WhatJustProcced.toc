## Interface: 50400
## Title: What Just Procced
## Notes: Provides training reminders to help decipher the Blizzard Spell Alerts.
## Author: Farmbuyer of US-Kilrogg
## OptionalDeps: Ace3
## X-Curse-Packaged-Version: v7
## X-Curse-Project-Name: What Just Procced
## X-Curse-Project-ID: what-just-procced
## X-Curse-Repository-ID: wow/what-just-procced/mainline

#@no-lib-strip@
libs\LibStub\LibStub.lua
libs\CallbackHandler-1.0\CallbackHandler-1.0.xml
libs\AceAddon-3.0\AceAddon-3.0.xml
libs\AceEvent-3.0\AceEvent-3.0.xml
libs\AceConsole-3.0\AceConsole-3.0.xml
#@end-no-lib-strip@

main.lua

