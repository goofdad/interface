## Interface: 50400
## Title: CalendarKeyboardFixer
## Description: Bug fix for the Blizzard calendar -- prevents unexpected window closure while editing events
## Notes: Bug fix for the Blizzard calendar -- prevents unexpected window closure while editing events
## Author: oscarucb
## Credits: 
## Version: 1.1.4
## X-Build: 20
## X-ReleaseDate: 2013-09-10T21:42:06Z
## X-Revision: $Revision: 20 $
## X-Curse-Packaged-Version: 1.1.4
## X-Curse-Project-Name: CalendarKeyboardFixer
## X-Curse-Project-ID: calendarkeyboardfixer
## X-Curse-Repository-ID: wow/calendarkeyboardfixer/mainline

CalendarKeyboardFixer.lua
