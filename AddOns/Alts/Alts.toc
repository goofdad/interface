## Interface: 50400
## Title: Alts
## Notes: Manages social information with a focus on main-alt associations.
## Author: Talryn
## Version: 1.2
## OptionalDeps: Ace3, LibDeformat-3.0, lib-st, LibDataBroker-1.1, LibDBIcon-1.0
## X-Embeds: Ace3
## X-Category: Interface Enhancements 
## SavedVariables: AltsDB
## X-Curse-Packaged-Version: 1.2
## X-Curse-Project-Name: Alts
## X-Curse-Project-ID: alts
## X-Curse-Repository-ID: wow/alts/mainline

embeds.xml

locale.xml

AltsDB.lua
Alts.lua
