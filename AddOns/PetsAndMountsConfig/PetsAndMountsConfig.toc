## Interface: 50400
## Title: Pets & Mounts Configuration
## Notes: Auto and random summon highly customizable for your pets and mounts, with Data Broker support.
## Author: Shenton
## LoadOnDemand: true
## Dependencies: PetsAndMounts
## OptionalDeps: Ace3
## X-Curse-Packaged-Version: v1.6.1
## X-Curse-Project-Name: Pets and Mounts
## X-Curse-Project-ID: pets-and-mounts
## X-Curse-Repository-ID: wow/pets-and-mounts/mainline

#@no-lib-strip@
Libs\AceDBOptions-3.0\AceDBOptions-3.0.xml
Libs\AceGUI-3.0\AceGUI-3.0.xml
Libs\AceConfig-3.0\AceConfig-3.0.xml
#@end-no-lib-strip@

Libs\AceGUIWidget-DropDown-SortByValue\Lib.xml
Libs\ForAllIndentsAndPurposes\ForAllIndentsAndPurposes.lua

Frames.xml

Core.lua
