## Interface: 50400
## Title: Paste
## Description: Enables multi-line/unlimited-length pasting of text or commands into WoW
## Notes: Enables multi-line/unlimited-length pasting of text or commands into WoW
## Author: oscarucb
## Credits: 
## Version: 1.3.1
## X-Build: 32
## X-ReleaseDate: 2014-04-03T04:37:56Z
## X-Revision: $Revision: 18 $
## SavedVariables: PasteDB 
## SavedVariablesPerCharacter: 
## X-Embeds: Ace3, LibDataBroker-1.1, LibDBIcon-1.0, LibStrataFix
## OptionalDeps: Ace3, LibDataBroker-1.1, LibDBIcon-1.0
## LoadManagers: AddonLoader
## X-LoadOn-Always: delayed
## X-LoadOn-Slash: /paste
## X-Curse-Packaged-Version: 1.3.1
## X-Curse-Project-Name: Paste
## X-Curse-Project-ID: paste
## X-Curse-Repository-ID: wow/paste/mainline

embeds.xml

locale.lua

core.lua
