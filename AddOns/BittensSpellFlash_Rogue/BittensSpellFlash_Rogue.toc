## Interface: 50400
## Version: 50400.1.3
## Author: Xemnosyst
## Title: Bitten's SpellFlash: Rogue
## Notes: Replaces Blizzard's default proc highlighting to flash a maximum dps rotation.
## URL: http://www.curse.com/addons/wow/bittens-spellflash-rogue
## Dependencies: SpellFlash
## OptionalDeps: BittensSpellFlashLibrary
## LoadOnDemand: 1
## X-SpellFlashAddon-LoadWith: Rogue
## X-Curse-Packaged-Version: 50400.1.3
## X-Curse-Project-Name: Bitten's SpellFlash: Rogue
## X-Curse-Project-ID: bittens-spellflash-rogue
## X-Curse-Repository-ID: wow/bittens-spellflash-rogue/mainline
utils\BittensUtils.xml
bsl\BittensSpellFlashLibrary.xml
src\Localization.lua
src\IDs.lua
src\Spells.lua
src\Rotations.lua
src\Options.lua
