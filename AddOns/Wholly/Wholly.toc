## Interface: 50400
## Title: Wholly
## Author: Scott M Harrison
## Notes: Shows quest database and map pins
## Version: 043
## Dependencies: Grail
## OptionalDeps: LibStub, LibDataBroker, TomTom, Gatherer, OmegaMap, Carbonite
## SavedVariablesPerCharacter: WhollyDatabase

Wholly.lua
Wholly.xml
