--Localization.deDE.lua
--Localization by 

if GetLocale() == "deDE" then

 RareAnnouncerLocalization = {
	-- ["71864"] = "",
	["71919"] = "Zhu-Gon den Sour", -- Needs review
	-- ["72045"] = "",
	-- ["72048"] = "",
	-- ["73854"] = "",
	-- ["72193"] = "",
	-- ["72245"] = "",
	["72769"] = "Geist der Jadefire", -- Needs review
	-- ["72775"] = "",
	-- ["72808"] = "",
	-- ["72909"] = "",
	-- ["72970"] = "",
	-- ["73157"] = "",
	["73158"] = "Smaragd Gander", -- Needs review
	-- ["73160"] = "",
	["73161"] = "Große Schildkröte Furyshell", -- Needs review
	-- ["73163"] = "",
	["73166"] = "Monströser Spineclaw", -- Needs review
	-- ["73167"] = "",
	["73169"] = "Jakur von Ordon", -- Needs review
	-- ["73170"] = "",
	["73171"] = "Meister von der Blackflame", -- Needs review
	-- ["73172"] = "",
	["73173"] = "Urdur der Cauterizer", -- Needs review
	["73174"] = "Archiereus der Flamme", -- Needs review
	-- ["73175"] = "",
	-- ["73277"] = "",
	-- ["73279"] = "",
	["73281"] = "Dread Schiff Vazuvius", -- Needs review
	-- ["73282"] = "",
	-- ["73704"] = "",
	Name = "Name",
  ago = "vor.", -- Needs review
	Alive = "Lebendig", -- Needs review
	announce = "Wenn Sie eine seltene ausrichten oder wenn ein selten stirbt ankündigen", -- Needs review
	Coordinates = "Koordinaten:", -- Needs review
	Dead = "Toten", -- Needs review
	["has died"] = "ist gestorben", -- Needs review
	hidetracking = "Blenden Sie Tracking aus, während im Kampf", -- Needs review
	Killed = "Getötet", -- Needs review
	["last killed"] = "wurde zuletzt getötet", -- Needs review
	-- optionsConfig = "",
	-- optionsTracking = "",
	recentkill = "vor weniger als einer Minute wurde zuletzt getötet werden.", -- Needs review
	respond = "Auf Anfragen zu Antworten ", -- Needs review
	responedRT = "Auf Raretime/Raretimer Anfragen zu Antworten", -- Needs review
	setTomTom = "TomTom Wegpunkt gesetzt, wenn ein seltener festgestellt, dass Sie nicht Zielen", -- Needs review
	-- trackNotice = "",
	-- trackSelectAll = "",
	-- trackSelectNone = "",
	-- trackTypeAll = "",
	-- trackTypeAllTooltip = "",
	-- trackTypeHead = "",
	-- trackTypeNext = "",
	-- trackTypeNextTooltip = "",
	-- trackTypeSelected = "",
	-- trackTypeSelectedTooltip = "",
}
end
