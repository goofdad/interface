== Version 50400.1.3
* All: Bugfix - the new Recuperate flashing was broken.

== Version 50400.1.2
* All: Added flashing for Recuperate out of combat when solo.
* All: Recuperate can now flashing during its last tick, since it will not be lost.
* All: Flashes WoW's 9th Anniversary Celebration Package.

== Version 50400.1.1
* Bugfix update to Bitten's SpellFlash Library.

== Version 50400.1.0
* Combat: Anticipates combo points from Ruthlessness.
* Combat: Use Marked for Death even with 1 combo point, since with Ruthlessness you will rarely be at 0.
* All: Slightly improved resource anticipation.

== Version 3.3.1
* All: Now flashes food buffs.
* All: Now flashes Shiv to dispel enrage effects.

== Version 3.3.0
Update for patch 5.3:
* Combat & Subtlety: Shuriken Toss is now only used when out of melee range (its dps benefit came from its low energy cost).
* All: The "No Backsab List" now includes Qiang the Merciless.

== Version 3.2.1
* Assassination: Bugfix - the Mutilate-before-Rupture priority was broken.
* All: Improved energy calculations when wearing 4pT15.
* All: The "No Backstab List" now includes Azure Serpent.

== Version 3.2.0
* All: Revisited all three rotations, updating them with Shadow Blades and level 90 talents, along with other changes from new research.
* Combat: Now tracks Bandit's Guile correctly when you miss.
* Subtlety: Improved energy prediction (w/ Energetic Recovery).

== Version 3.1.3
NOTE: This version includes a new version of Bitten's SpellFlash Library that I tried to test well, but if you find any issues please let me know right away.
* Combat: Sinister Strike will no longer flash outside of melee range.  This release is mostly about the update to the library.

== Version 3.1.2
* Assassination: No longer pause at 4 combo points to "refresh" Rupture if it is not up.
* Combat: Never suggests a finisher when Revealing Strike is down.
* Combat: Never tries to pool energy when solo.
* Combat: Never tries to apply Slice and Dice early to last through Deep Insight when solo.
* Combat & Subtlety: Don't flash Slice and Dice w/ less than 5 combo points when solo and you have Glyph of Deadly Momentum.
* All: Added Recuperate flashing when solo and you have Glyph of Deadly Momentum.
* All: Bugfix - Flashing Deadly Poison was broken.

== Version 3.1.1
* Missed Paralytic Poison in the last version.

== Version 3.1.0
NOTE: Your options will be reset when you upgrade to this version.
* Repackaging so that Bitten's SpellFlash Library is included with the addon, instead of downloaded separately.
* Several other internal changes.
* Combat & Subtlety: No longer flashes Ambush if you have Cloak and Dagger and the enemy is in the (resurrected) "No Shadowstep" list.
* All: Added Marked for Death.
* All: Added flashing for Non-Lethal Poisons.
* All: No longer insists that you switch from Wound to Deadly Poison.

== Version 3.0.0
* Combat: Added a Combat rotation.
* Subtlety: Bugfix - the energy from Relentless Strikes was not being anticipated for Eviscerate.
* Subtlety: Bugfix - Shadow Dance and Preparation should have been flashing yellow.  Now they are.
* All: Added Expose Armor.

== Version 2.4.3
* Making the "Out of Date" status go away.  Please report any real issues w/ the new patch.

== Version 2.4.2
* Subtlety: The detection of the glyph "Tricks of the Trade" and the talent "Subterfuge" were broken.
* All: Removed chat frame spam during combat associated with the latest Bitten's SpellFlash Library release (which is what led me to find the bugs listed above).

== Version 2.4.1
* Bugfix: The options were messed up.  Should be straightened out now.

== Version 2.4.0
* Assassination: Added an Assassination rotation.
* Subtlety: Played with when Tricks of the Trade flashes.
* Subtlety: Added Proto-Drake Egg to the list of things not to backstab.

== Version 2.3.0
* Added a Subtlety rotation for the MoP beta.

== Version 2.2.1
* Added the 2nd Burning Tendon to the Shadowstep blacklist.

== Version 2.2.0
* Assassination/Subtlety: Improved flashing during Vanish.
* Assassination/Subtlety: There is now a blacklist of mobs on which to flash Backstab (like Ultraxion).  Please see the description to see what they are, and leave comments to have it adjusted.
* Combat: There is now a blacklist of mobs on which to flash Killing Spree (like Deathwing's Head).  Please see the description to see what they are, and leave comments to have it adjusted.
* Subtlety: There is now a blacklist of mobs on which to flash Shadowstep (like Ultraxion).  Please see the description to see what they are, and leave comments to have it adjusted.
